Thang Le Duc, Long Hoang

# Semestrální práce OMO - Smart Home 2023/2024

Zadáním je ytvořit aplikaci pro virtuální simulaci inteligentního domu, kde simulujeme chod domácnosti, používáme jednotlivá zařízení domu a vyhodnocujeme využití, spotřebu, volný a pracovní čas jednotlivých osob.

# Funkční požadavky

1. Entity, se kterými pracujeme, jsou dům, okno (+venkovní žaluzie), patro v domě, senzor, zařízení (=spotřebič), osoba, auto, kolo, domácí zvíře a libovolné další entity
2. Jednotlivá zařízení v domu mají API na ovládání. Zařízení mají stav, který lze měnit pomocí API na jeho ovládání. Akce z API jsou použitelné podle stavu zařízení. Vybraná zařízení mohou mít i obsah - lednice má jídlo, CD přehrávač má CD.
3. Spotřebiče mají svojí spotřebu v aktivním stavu, idle stavu, vypnutém stavu
4. Jednotlivá zařízení mají API na sběr dat o tomto zařízení. O zařízeních sbíráme data jako spotřeba elektřiny, plynu, vody a funkčnost (klesá lineárně s časem)
5. Jednotlivé osoby a zvířata mohou provádět aktivity (akce), které mají nějaký efekt na zařízení nebo jinou osobu.
6. Jednotlivá zařízení a osoby se v každém okamžiku vyskytují v jedné místnosti (pokud nesportují) a náhodně generují eventy (eventem může být důležitá informace a nebo alert).
7. Eventy jsou přebírány a odbavovány vhodnou osobou (osobami) nebo zařízením (zařízeními). Např.:
  - _čidlo na vítr (vítr) =\> vytažení venkovních žaluzií_
  - _jistič (výpadek elektřiny) =\> vypnutí všech nedůležitých spotřebičů (v provozu zůstávají pouze ty nutné)_
  - _čidlo na vlhkost (prasklá trubka na vodu) =\> máma -\> zavolání hasičů, táta -\> uzavření vody, dcera -\> vylovení křečka_
8. Vygenerování reportů:
  - _HouseConfigurationReport_: veškerá konfigurační data domu zachovávající hierarchii - dům -\> patro -\> místnost -\> okno -\> žaluzie atd. Plus jací jsou obyvatelé domu.
  - _EventReport_: report eventů, kde grupujeme eventy podle typu, zdroje eventů a jejich cíle (jaká entita event odbavila)
  - _ActivityAndUsageReport_: Report akcí (aktivit) jednotlivých osob a zvířat, kolikrát které osoby použily které zařízení.
  - _ConsumptionReport_: Kolik jednotlivé spotřebiče spotřebovaly elektřiny, plynu, vody. Včetně finančního vyčíslení.
9. Při rozbití zařízení musí obyvatel domu prozkoumat dokumentaci k zařízení - najít záruční list, projít manuál na opravu a provést nápravnou akcí (např. Oprava svépomocí, koupě nového atd.). Manuály zabírají mnoho místa a trvá dlouho, než je najdete.
10. Rodina je aktivní a volný čas tráví zhruba v poměru (50% používání spotřebičů v domě a 50% sport kdy používá sportovní náčiní kolo nebo lyže). Když není volné zařízení nebo sportovní náčiní, tak osoba čeká.

# Nefunkční požadavky

- Není požadována autentizace ani autorizace
- Aplikace může běžet pouze v jedné JVM
- Aplikaci pište tak, aby byly dobře schované metody a proměnné, které nemají být dostupné ostatním třídám. Vygenerovaný javadoc má mít co nejméně public věcí
- Reporty jsou generovány do textového souboru
- Konfigurace domu, zařízení a obyvatel domu může být nahrávána přímo z třídy nebo externího souboru (preferován je json)

# Vhodné design patterny

- State machine
- Iterator
- Factory/Factory method
- Decorator/Composite
- Singleton
- Visitor/Observer/Listener
- Chain of responsibility
- Partially persistent data structure
- Object Pool
- Lazy Initialization

# Požadované výstupy

- Design ve formě use case diagramů, class diagramů a stručného popisu jak chcete úlohu realizovat
- Veřejné API - Javadoc vygenerovaný pro funkce, kterými uživatel pracuje s vaším software
- Dvě různé konfigurace domu a pro ně vygenerovány reporty za různá období. Minimální konfigurace alespoň jednoho domu je: 6 osob, 3 zvířata, 8 typů spotřebičů, 20 ks spotřebičů, 6 místností, jedny lyže, dvě kola.

# Použité design patterny

**CREATIONAL**

- **Factory method** - při tvorbě lidí, zařízení a domácích mazlíčků
- **Singleton** - vytvoření slunce
- **Builder** - na stavbu patra

**STRUCTURAL**

- **Composite** – rozdělení domu
- **Adapter** - pro komunikaci domu se sluncem
- **Facade** – interakce člověka se zařízeními

**BEHAVIORAL**

- **Observer** - nastane-li nějaký Event, zařízení změní svůj stav
- **Visitor** - manipulaci lidí s určitými zařízeními v domácnosti
- **State** - pro manipulaci stavů každého zařízení

# Spuštění simulace
Po spuštění Main class se spustí simulace, která:
- Náhodnému člověku vygeneruje aktivitu s náhodným přístrojem
- Vygeneruje náhodný Event
- Náhodnému člověku vygeneruje náhodný dopravní prostředek, který využije

# Diagramy
![model](src/main/resources/diagram.png)
![usecase](src/main/resources/usecase.png)




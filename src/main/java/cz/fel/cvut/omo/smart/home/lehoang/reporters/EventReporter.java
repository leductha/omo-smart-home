package cz.fel.cvut.omo.smart.home.lehoang.reporters;

import cz.fel.cvut.omo.smart.home.lehoang.device.Blinds;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.device.Heater;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.event.TemperatureEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.WindEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.HungerEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.PowerEvent;
import cz.fel.cvut.omo.smart.home.lehoang.house.House;
import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class EventReporter {
    private static final Logger LOG = Logger.getLogger(EventReporter.class.getName());

    List<Event> events = new ArrayList<>();

    public List<Event> getEvents() {
        return events;
    }

    private static final String FOLDER_PATH = "src/main/java/cz/fel/cvut/omo/smart/home/lehoang/reporters/reports";

    private static final String FILE_NAME = "event.txt";

    private static final String PATH = FOLDER_PATH +"/"+ FILE_NAME;

    public void create(House house) {
        try {
            File myObj = new File(PATH);
            if (myObj.createNewFile()) {
                LOG.info("File created: " + myObj.getName());
            }
        } catch (IOException e) {
            LOG.info("An error occurred.");
            e.printStackTrace();
        }


        try (FileWriter fw = new FileWriter(PATH)) {

            fw.write("+--------------+ \n");
            fw.write("| EVENT REPORT | \n");
            fw.write("+--------------+ \n");
            fw.write("// Creator, Reactor, Action, Value \n");
            fw.write("\n");

            for (Event event : house.getEventReporter().getEvents()) {
                if (event instanceof WindEvent) {
                    WindEvent windEvent = (WindEvent) event;
                    Blinds blinds = (Blinds) windEvent.getReactor();

                    fw.write("[" + windEvent.getName() + "]" + " " + windEvent.getCreator().getClass().getSimpleName() + ", " + blinds.getName() + blinds.getDeviceId() + " " + windEvent.getDescription() + ", Wind: " + windEvent.getWind() + "\n");
                } else if (event instanceof TemperatureEvent) {
                    TemperatureEvent temperatureEvent = (TemperatureEvent) event;
                    Heater heater = (Heater) temperatureEvent.getReactor();

                    fw.write("[" + temperatureEvent.getName() + "]" + " " + temperatureEvent.getCreator().getClass().getSimpleName() + " " + heater.getName() + heater.getDeviceId() + " " + temperatureEvent.getDescription() + ", Temperature: " + temperatureEvent.getTemp() + "\n");
                } else if (event instanceof HungerEvent) {
                    HungerEvent hungerEvent = (HungerEvent) event;
                    Pet pet = (Pet) hungerEvent.getCreator();
                    fw.write("[" + hungerEvent.getName() + "] " + pet.getName() + " is hungry" + "\n");
                } else if (event instanceof PowerEvent) {
                    PowerEvent powerEvent = (PowerEvent) event;
                    Device device = (Device) powerEvent.getReactor();
                    fw.write("[" + powerEvent.getName() + "] " + device.getName() + device.getDeviceId() + " was turned off" + "\n");
                }
            }


        } catch (IOException e) {
            LOG.info("Failed writing into file " + FILE_NAME);
            e.printStackTrace();
        }
    }


    public void add(Event event) {
        if (event instanceof WindEvent windEvent) {
            events.add(windEvent.clone());
        } else if (event instanceof PowerEvent powerEvent) {
            events.add(powerEvent.clone());
        } else {
            events.add(event);
        }
    }
}

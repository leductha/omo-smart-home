package cz.fel.cvut.omo.smart.home.lehoang.people;

import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class Daughter extends Person{

    public Daughter(String name, int age, Room room) {
        super(name, age, room);
    }
}

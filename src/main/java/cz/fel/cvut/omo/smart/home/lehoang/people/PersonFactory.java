package cz.fel.cvut.omo.smart.home.lehoang.people;

import cz.fel.cvut.omo.smart.home.lehoang.event.MoveEvent;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.*;

public class PersonFactory {
    public Person createPerson(PersonType type, String name, int age, Room room) {
        return switch (type) {
            case DAD -> createDad(name, age, room);
            case MUM -> createMum(name, age, room);
            case SON -> createSon(name, age, room);
            case DAUGHTER -> createDaughter(name, age, room);
            default -> throw new IllegalArgumentException("Invalid person type");
        };
    }

    private Person createDad(String name, int age, Room room) {
        Person dad = new Dad(name, age, room);
        room.addPerson(dad);

        for (Sensor s : room.getHouse().getAllSensors()) {
            if (s instanceof BreakSensor){
                s.registerObserver(dad);
            }
        }

        room.findDeviceName("Lights").update(new MoveEvent("Spawned in room"));
        return dad;
    }

    private Person createMum(String name, int age, Room room) {
        Person mum = new Mum(name, age, room);
        room.addPerson(mum);
        room.findDeviceName("Lights").update(new MoveEvent("Spawned in room"));
        return mum;
    }

    private Person createSon(String name, int age, Room room) {
        Person son = new Son(name, age, room);
        room.addPerson(son);
        room.findDeviceName("Lights").update(new MoveEvent("Spawned in room"));
        return son;
    }

    private Person createDaughter(String name, int age, Room room) {
        Person daughter = new Daughter(name, age, room);
        room.addPerson(daughter);
        room.findDeviceName("Lights").update(new MoveEvent("Spawned in room"));
        return daughter;
    }
}


package cz.fel.cvut.omo.smart.home.lehoang.people;

import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class Dad extends Person{
    public Dad(String name, int age, Room room) {
        super(name, age, room);
    }
}

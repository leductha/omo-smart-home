package cz.fel.cvut.omo.smart.home.lehoang.event;

public class MoveEvent extends Event{
    public MoveEvent(String name) {
        super(name);
    }

    public MoveEvent(MoveEvent moveEvent) {
        super(moveEvent);
    }

    @Override
    public Event clone() {
        return new MoveEvent(this);
    }

    @Override
    public String toString() {
        return getName() + ", Reactor: " + getReactor() + ", Creator: " + getCreator();
    }
}

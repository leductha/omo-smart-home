package cz.fel.cvut.omo.smart.home.lehoang;


public class Main {
    public static void main(String[] args) {
        Simulation simulation = new Simulation();
        simulation.simulate(10);
        simulation.generateReports();
    }
}

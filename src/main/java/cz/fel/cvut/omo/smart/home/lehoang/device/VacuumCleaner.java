package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;

public class VacuumCleaner extends Device {
    private Person person;

    public VacuumCleaner(int id, String name, Room room) {
        super(id, name, room);
    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }

    public void clean (Person person, Room room) {
        this.getRoom().removeDevice(this);
        room.addDevice(this);
        person.moveTo(room);

        this.person = person;
        this.setRoom(room);
    }



    public Person getPerson() {
        return person;
    }
}

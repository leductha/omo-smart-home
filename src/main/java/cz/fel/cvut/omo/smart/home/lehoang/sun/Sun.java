package cz.fel.cvut.omo.smart.home.lehoang.sun;

import java.util.Random;
// device
public class Sun {
    private int sunrays;

    public Sun() {
        Random random = new Random();
        sunrays = random.nextInt(-50,5000);
    }

    public int getSunrays() {
        return sunrays;
    }
}

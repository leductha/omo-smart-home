package cz.fel.cvut.omo.smart.home.lehoang.pets;

import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.people.Dad;
import cz.fel.cvut.omo.smart.home.lehoang.people.Mum;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;

public class PetFactory {
    public Pet createPet(PetType type, String name, int age, Room room) {
        return switch (type) {
            case CAT -> createCat(name, age, room);
            case DOG -> createDog(name, age, room);
            default -> throw new IllegalArgumentException("Invalid animal type");
        };
    }

    private Pet createCat(String name, int age, Room room) {
        Pet cat = new Cat(name, age, room);
        room.addPet(cat);

        for (Person person : room.getHouse().getAllPeople()) {
            if (person instanceof Dad || person instanceof Mum) {
                cat.registerObserver(person);
            }
        }
        return cat;
    }

    private Pet createDog(String name, int age, Room room) {
        Pet dog = new Dog(name, age, room);
        room.addPet(dog);

        for (Person person : room.getHouse().getAllPeople()) {
            if (person instanceof Dad || person instanceof Mum) {
                dog.registerObserver(person);
            }
        }
        return dog;
    }


}

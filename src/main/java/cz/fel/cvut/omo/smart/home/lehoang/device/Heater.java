package cz.fel.cvut.omo.smart.home.lehoang.device;


import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OnState;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OffState;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.event.PowerEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.TemperatureEvent;

public class Heater extends Device {
    public Heater(int id, String name, Room room) {
        super(id, name, room);
    }

    @Override
    public void update(Event e) {

        if (e instanceof PowerEvent && this.getState() instanceof OnState) {
            turnOff();

            e.setReactor(this);
            getEventReporter().add(e);

            getActivityAndUsageReporter().addActivity(this.getName() + this.getDeviceId() + " was turned OFF due to blow fuses");
        }

        if (e instanceof TemperatureEvent temperatureevent) {
            if (temperatureevent.getTemp() < 25 && this.getState() instanceof OffState) {
                turnOn();
                setPowerConsumed(getPowerConsumed() + 1.5);
                e.setDescription("was turned ON");
                use();
            } else if (temperatureevent.getTemp() >= 25 && this.getState() instanceof OnState) {
                turnOff();
                e.setDescription("was turned OFF");
            } else {
                e.setDescription("did nothing");
            }
            e.setReactor(this);
            getEventReporter().add(e);
            this.getRoom().getTemperatureSensor().setTemperatureInside(temperatureevent.getTemp());
        }

    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }
}

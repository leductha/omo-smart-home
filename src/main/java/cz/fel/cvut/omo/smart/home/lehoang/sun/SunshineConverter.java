package cz.fel.cvut.omo.smart.home.lehoang.sun;

public interface SunshineConverter {
    int convertToTemperature();
}

package cz.fel.cvut.omo.smart.home.lehoang.pets;

import cz.fel.cvut.omo.smart.home.lehoang.device.Observer;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.Subject;

import java.util.ArrayList;
import java.util.List;
import cz.fel.cvut.omo.smart.home.lehoang.event.HungerEvent;

import java.util.Random;


public abstract class Pet implements Subject {
    private List<Observer> observers = new ArrayList<>(); // PARENTS

    protected String name;
    protected int age;
    protected Room room;

    public Pet(String name, int age, Room room) {
        this.name = name;
        this.age = age;
        this.room = room;
    }

    public abstract void makeSound();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " Name: " + this.name + ", Age: " + this.age + ", Room: " + this.room.getName();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int index = observers.indexOf(o);
        if (index >= 0) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObservers(Event event) {
        for (Observer observer : observers) {
            observer.update(event);
        }
    }

    public void notifyObserver(Event event, Observer observer) {
        observer.update(event);
    }

    public void triggerHunger() {
        Random random = new Random();
        int randomNumber = random.nextInt(observers.size());

        HungerEvent newEvent = new HungerEvent("Hunger Event");
        newEvent.setCreator(this);
        notifyObserver(newEvent, observers.get(randomNumber));
    }

    public List<Observer> getObservers() {
        return observers;
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.event;

public interface Prototype {
    Event clone();

}

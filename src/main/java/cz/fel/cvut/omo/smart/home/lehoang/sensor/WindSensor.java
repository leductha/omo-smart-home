package cz.fel.cvut.omo.smart.home.lehoang.sensor;
import cz.fel.cvut.omo.smart.home.lehoang.device.Observer;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.event.WindEvent;

import java.util.ArrayList;
import java.util.List;

public class WindSensor extends Sensor {
    private float windSpeed;
    private List<Observer> observers = new ArrayList<>();
    private Room room;

    public WindSensor(float windSpeed, Room room) {
        this.windSpeed = windSpeed;
        this.room = room;
    }
    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int index = observers.indexOf(o);
        if (index >= 0) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObservers(Event event) {
        for (Observer observer : observers) {
            observer.update(event);
        }
    }

    public Event generateEvent() {
        float randomWIndSpeed = (float) Math.random() * 100;
        WindEvent newEvent = new WindEvent("Wind Event", (int) randomWIndSpeed);
        newEvent.setCreator(this);
        for (Sensor s : room.getFloor().getHouse().getAllSensors()) {
            if (s instanceof WindSensor){
                s.notifyObservers(newEvent);
            }
        }
        return newEvent;
    }

    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    public float getWindSpeed() {
        return this.windSpeed;
    }

    @Override
    public String toString() {
        return "WindSensor {" +
                "windSpeed=" + windSpeed +
                ", observers=" + observers +
                ", room=" + room +
                '}';
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.pets;

import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class Cat extends Pet {


    public Cat(String name, int age, Room room) {
        super(name, age, room);
    }

    @Override
    public void makeSound() {
        System.out.println("meow");
    }

}

package cz.fel.cvut.omo.smart.home.lehoang.event;

import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class TemperatureEvent extends Event {
    private int temp;


    public TemperatureEvent(String name, Room room, int temp) {
        super(name);
        this.temp = temp;
        this.room = room;
    }

    public TemperatureEvent(TemperatureEvent temperatureEvent) {
        super(temperatureEvent);
    }

    public int getTemp() {
        return temp;
    }

    @Override
    public String toString() {
        return getName() + ", Reactor: " + getReactor() + ", " + getDescription() + ", Temperature: " + temp;
    }

    @Override
    public TemperatureEvent clone() {
        return new TemperatureEvent(this);
    }
}

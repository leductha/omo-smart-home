package cz.fel.cvut.omo.smart.home.lehoang.house;

import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.*;
import cz.fel.cvut.omo.smart.home.lehoang.device.Blinds;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.device.Lights;


import java.util.ArrayList;
import java.util.List;

public class Room extends AbstractArea {
    private Floor floor;

    private boolean windows;

    private final ArrayList<Person> people;

    private final ArrayList<Device> devices;

    private final ArrayList<Sensor> sensors;

    private final ArrayList<Pet> pets;

    private static int blindsId = 1;
    private static int lightsId = 1;

    public Room(String name, Floor floor, boolean windows) {
        super(name);
        this.floor = floor;
        this.devices = new ArrayList<>();
        this.people = new ArrayList<>();
        this.pets = new ArrayList<>();
        this.sensors = new ArrayList<>();
        this.windows = windows;
        addSensors();
    }

    public void addSensors() {
        sensors.add(new PowerSensor(this));
        sensors.add(new BreakSensor(this));
        this.addMoveSensor();
        if (windows) {
            this.addWindSensor();
        }
    }


    public Floor getFloor() {
        return this.floor;
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }

    public void addDevice(Device device) {
        this.devices.add(device);
    }

    public void removeDevice(Device device) {
        this.devices.remove(device);
    }

    public void addPerson(Person person) {
        this.people.add(person);
    }

    public void removePerson(Person person) {
        this.people.remove(person);
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
    }

    public void removePet(Pet pet) {
        this.pets.remove(pet);
    }

    public void addSensor(Sensor sensor) {
        this.sensors.add(sensor);
    }

    public void removeSensor(Sensor sensor) {
        this.sensors.remove(sensor);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Person> getAllPeople() {
        return people;
    }

    @Override
    public List<Pet> getAllPets() {
        return pets;
    }

    @Override
    public List<Device> getAllDevices() {
        return devices;
    }

    @Override
    public List<Sensor> getAllSensors() {
        return sensors;
    }

    @Override
    public String toString() {
        return "Room name: " + name;
    }

    public boolean hasSensor(Class<? extends Sensor> clazz) {
        for (Sensor sensor : sensors) {
            if (clazz.isInstance(sensor)) {
                return true;
            }
        }
        return false;
    }

    public PowerSensor getPowerSensor() {
        for (Sensor sensor : sensors) {
            if (sensor instanceof PowerSensor powersensor) {
                return powersensor;
            }
        }
        return null;
    }

    public TemperatureSensor getTemperatureSensor() {
        for (Sensor sensor : sensors) {
            if (sensor instanceof TemperatureSensor temperaturesensor) {
                return temperaturesensor;
            }
        }
        return null;
    }

    public WindSensor getWindSensor() {
        for (Sensor sensor : sensors) {
            if (sensor instanceof WindSensor windSensor) {
                return windSensor;
            }
        }
        return null;
    }


    public Device findDeviceByIdAndName(int id, String name) {
        for (Device device : devices) {
            if (device.getDeviceId() == id && device.getName().equals(name)) {
                return device;
            }
        }

        return null;
    }

    public House getHouse() {
        return this.floor.getHouse();
    }

    public MoveSensor getMoveSensor() {
        for (Sensor s : sensors) {
            if (s instanceof MoveSensor moveSensor) {
                return moveSensor;
            }
        }

        return null;
    }

    public BreakSensor getBreakSensor() {
        for (Sensor s : sensors) {
            if (s instanceof BreakSensor breakSensor) {
                return breakSensor;
            }
        }

        return null;
    }

    public Device findDeviceName(String name) {
        for (Device device : devices) {
            if (device.getName().equals(name)) {
                return device;
            }
        }

        return null;
    }

    public void addWindSensor() {
        Blinds blind = new Blinds(blindsId, "Blinds", this);
        blindsId++;
        devices.add(blind);
        sensors.add(new WindSensor(0, this));
        this.getWindSensor().registerObserver(blind);
        this.getPowerSensor().registerObserver(blind);
    }


    public void addMoveSensor() {
        Lights lights = new Lights(lightsId, "Lights", this);
        lightsId++;
        devices.add(lights);
        sensors.add(new MoveSensor(this));
        this.getMoveSensor().registerObserver(lights);
        this.getPowerSensor().registerObserver(lights);
    }
}

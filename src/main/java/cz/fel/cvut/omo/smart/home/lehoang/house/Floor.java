package cz.fel.cvut.omo.smart.home.lehoang.house;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;
import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.Sensor;

import java.util.ArrayList;
import java.util.List;

public class Floor extends AbstractArea{
    private int level;

    private House house;

    private final List<Room> rooms;

    public Floor(String name, int level, House house) {
        super(name);
        this.level = level;
        this.house = house;
        this.rooms = new ArrayList<>();
    }

    public int getLevel() {
        return level;
    }

    public void setLevel (int level) {
        this.level = level;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void addRoom(Room newRoom) {
        rooms.add(newRoom);
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Person> getAllPeople() {
        return getRooms()
                .stream()
                .flatMap(room -> room.getAllPeople().stream())
                .toList();
    }

    @Override
    public List<Pet> getAllPets() {
        return getRooms()
                .stream()
                .flatMap(room -> room.getAllPets().stream())
                .toList();
    }

    @Override
    public List<Device> getAllDevices() {
        return getRooms()
                .stream()
                .flatMap(room -> room.getAllDevices().stream())
                .toList();
    }

    @Override
    public List<Sensor> getAllSensors() {
        return getRooms().stream()
                .flatMap(room -> room.getAllSensors().stream())
                .toList();
    }

    @Override
    public String toString() {
        return "Floor name: " + name + ", level: " + level;
    }
}

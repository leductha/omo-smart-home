package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class CDPlayer extends Device{

    public CDPlayer(int id, String name, Room room) {
        super(id, name, room);
    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }

    public void playMusic() {
        setPowerConsumed(getPowerConsumed() + 0.05);
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.event;

public class HungerEvent extends Event {
    public HungerEvent(String name) {
        super(name);
    }

    public HungerEvent(HungerEvent hungerEvent) {
        super(hungerEvent);
    }

    @Override
    public Event clone() {
        return new HungerEvent(this);
    }
}

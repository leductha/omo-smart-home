package cz.fel.cvut.omo.smart.home.lehoang.house;

import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.Sensor;

import java.util.List;

public interface LivingArea {

    String getName();
    List<Person> getAllPeople();
    List<Pet> getAllPets();
    List<Device> getAllDevices();
    List<Sensor> getAllSensors();

}

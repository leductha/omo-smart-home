package cz.fel.cvut.omo.smart.home.lehoang.house;

public abstract class AbstractArea implements LivingArea {
    protected String name;

    protected AbstractArea(String name) {
        this.name = name;
    }
}

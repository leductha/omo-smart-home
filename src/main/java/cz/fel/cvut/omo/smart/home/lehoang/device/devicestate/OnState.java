package cz.fel.cvut.omo.smart.home.lehoang.device.devicestate;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;

public class OnState extends DeviceState{
    public OnState(Device device) {
        super(device);
    }

    public void breakDevice() {
        device.changeState(new BreakState(device));
    }

    public void turnOn() {
        device.changeState(new OnState(device));
    }

    public void turnOff() {
        device.changeState(new OffState(device));
    }

    public void turnIdle() {
        device.changeState(new IdleState(device));
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.device.devicestate;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.device.Lights;

public class OffState extends DeviceState {

    public OffState(Device device) {
        super(device);
    }

    public void breakDevice() {
        device.changeState(new BreakState(device));
    }

    public void turnOn() {
        if (Math.random() < (float) device.getChanceOfBreak()/100 && !(device instanceof Lights)) { // 5% chance of setting to BreakState, Lights are not included
            device.changeState(new BreakState(device));
            device.getRoom().getBreakSensor().notifyBroken(device);
        } else {
            device.changeState(new OnState(device));
        }
    }

    public void turnOff() {
        device.changeState(new OffState(device));
    }

    public void turnIdle() {
        device.changeState(new IdleState(device));
    }
}

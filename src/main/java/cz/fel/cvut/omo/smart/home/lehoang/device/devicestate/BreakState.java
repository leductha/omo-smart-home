package cz.fel.cvut.omo.smart.home.lehoang.device.devicestate;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;

public class BreakState extends DeviceState{
    public BreakState(Device device) {
        super(device);
    }

    public void breakDevice() {
        device.changeState(new BreakState(device));
    }

    public void turnOn() {
        if (Math.random() < (float) device.getChanceOfBreak()/100) { // 5% chance of setting to BreakState
            device.changeState(new BreakState(device));
            device.getRoom().getBreakSensor().notifyBroken(device);
        } else {
            device.changeState(new OnState(device));
        }
    }

    public void turnOff() {
        device.changeState(new OffState(device));
    }

    public void turnIdle() {
        device.changeState(new IdleState(device));
    }
}

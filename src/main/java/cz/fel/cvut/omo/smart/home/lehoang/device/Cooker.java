package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class Cooker extends Device{
    private int heat;


    public Cooker(int id, String name, Room room) {
        super(id, name, room);
        this.heat = 0;
    }

    public void setHeat(int heat) {
        this.heat = heat;
    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }

    public int getHeat() {
        return this.heat;
    }
}

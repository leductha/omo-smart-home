package cz.fel.cvut.omo.smart.home.lehoang.vehicles;

public enum VehicleType {
    BICYCLE,
    CAR,
    SKI,
    MOTORBIKE
}

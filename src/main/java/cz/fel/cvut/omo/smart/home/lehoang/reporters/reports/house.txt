+----------------------------+ 
| HOUSE CONFIGURATION REPORT | 
+----------------------------+ 
House name Intelligentes Zuhause, id: 69
----------------------------------------- 
Floor name: Grund Floor, level: 0
  Room name: Küche
    DEVICES: 
      Lights49 Room name: Küche
      PC2 Room name: Küche
    PEOPLE: 
    PETS: 
----------------------------------------- 
  Room name: Büro
    DEVICES: 
      Lights50 Room name: Büro
      TV1 Room name: Büro
      TV2 Room name: Büro
    PEOPLE: 
    PETS: 
----------------------------------------- 
  Room name: Schlafzimmer
    DEVICES: 
      Lights51 Room name: Schlafzimmer
      Blinds31 Room name: Schlafzimmer
      Fridge1 Room name: Schlafzimmer
    PEOPLE: 
    PETS: 
      Cat Name: Snowball II, Age: 1, Room: Schlafzimmer
----------------------------------------- 
  Room name: Badezimmer
    DEVICES: 
      Lights52 Room name: Badezimmer
      Blinds32 Room name: Badezimmer
      Fridge2 Room name: Badezimmer
    PEOPLE: 
      Mum Name: Marge, Age: 34, Room: Badezimmer
    PETS: 
      Dog Name: Santa's Little Helper, Age: 2, Room: Badezimmer
----------------------------------------- 
Floor name: Erster Stock, level: 1
  Room name: Jungenzimmer
    DEVICES: 
      Lights53 Room name: Jungenzimmer
      Blinds33 Room name: Jungenzimmer
      Microwave1 Room name: Jungenzimmer
    PEOPLE: 
      Dad Name: Homer, Age: 36, Room: Jungenzimmer
      Daughter Name: Maggie, Age: 1, Room: Jungenzimmer
    PETS: 
----------------------------------------- 
  Room name: Elternschlafzimmer
    DEVICES: 
      Lights54 Room name: Elternschlafzimmer
      Blinds34 Room name: Elternschlafzimmer
      CDPlayer1 Room name: Elternschlafzimmer
      Heater1 Room name: Elternschlafzimmer
    PEOPLE: 
    PETS: 
----------------------------------------- 
  Room name: Toilette
    DEVICES: 
      Lights55 Room name: Toilette
      Blinds35 Room name: Toilette
      VacuumCleaner1 Room name: Toilette
      Heater3 Room name: Toilette
    PEOPLE: 
      Son Name: Lisa, Age: 8, Room: Toilette
    PETS: 
----------------------------------------- 
  Room name: Wohnzimmer
    DEVICES: 
      Lights56 Room name: Wohnzimmer
      Heater2 Room name: Wohnzimmer
      Cooker1 Room name: Wohnzimmer
      PC1 Room name: Wohnzimmer
    PEOPLE: 
      Son Name: Bart, Age: 10, Room: Wohnzimmer
      Dad Name: Jack, Age: 50, Room: Wohnzimmer
    PETS: 
----------------------------------------- 

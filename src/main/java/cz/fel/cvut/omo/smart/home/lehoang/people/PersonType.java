package cz.fel.cvut.omo.smart.home.lehoang.people;

public enum PersonType {
    SON,
    DAUGHTER,
    DAD,
    MUM
}

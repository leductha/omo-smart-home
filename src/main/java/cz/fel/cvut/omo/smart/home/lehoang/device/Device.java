package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.DeviceState;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.IdleState;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OffState;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OnState;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Visitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.ActivityAndUsageReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.EventReporter;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.event.MoveEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.PowerEvent;


public abstract class Device implements Observer, Visitor {

    private int deviceId;
    private String name;
    private double powerConsumed;
    private int timesUsed;

    private int chanceOfBreak; // % of breaking when turning ON

    private DeviceState state;
    private Room room;

    protected Device (int id, String name, Room room) {
        this.deviceId = id;
        this.name = name;
        this.powerConsumed = 0;
        this.timesUsed = 0;
        this.chanceOfBreak = 10;
        this.state = new OffState(this);
        this.room = room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void changeState(DeviceState state) {
        this.state = state;
    }

    public void turnOff() {
        state.turnOff();
    }

    public void turnOn() {
        state.turnOn();
    }

    public void turnIdle() {
        state.turnIdle();
    }

    public void turnBroken() {
        state.breakDevice();
    }


    public DeviceState getState() {
        return state;
    }

    @Override
    public void update(Event e) {
        if (e instanceof PowerEvent && this.getState() instanceof OnState) {
            turnOff();
            e.setReactor(this);
            getEventReporter().add(e);
            getActivityAndUsageReporter().addActivity(this.getName() + this.getDeviceId() + " was turned OFF due to blow fuses");
        }
        else if (e instanceof MoveEvent) {
            if (this.room.getAllPeople().isEmpty() && this.state instanceof OnState) {
                turnIdle();
                setPowerConsumed(getPowerConsumed() + 0.01);
            }
            else if (!(this.room.getAllPeople().isEmpty()) && this.state instanceof IdleState) {
                turnOn();
                setPowerConsumed(getPowerConsumed() + 0.1);
                use();
            }
        }
    }

    public int getDeviceId() {
        return deviceId;
    }

    public String getName() {
        return name;
    }

    public int getTimesUsed() {
        return timesUsed;
    }

    public void setTimesUsed(int time) {
        this.timesUsed = time;
    }

    public void setPowerConsumed(double power) {
        this.powerConsumed = power;
    }

    public double getPowerConsumed() {
        return this.powerConsumed;
    }

    public Room getRoom() {
        return this.room;
    }

    public void use() {
        setTimesUsed(getTimesUsed() + 1);
    }

    public EventReporter getEventReporter() {
        return getRoom().getFloor().getHouse().getEventReporter();
    }

    public ActivityAndUsageReporter getActivityAndUsageReporter() {
        return getRoom().getFloor().getHouse().getActivityAndUsageReporter();
    }

    public int getChanceOfBreak() {
        return chanceOfBreak;
    }

    public void setChanceOfBreak(int chanceOfBreak) {
        this.chanceOfBreak = chanceOfBreak;
    }

    @Override
    public String toString() {
        return name + deviceId + " " + room;
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class PC extends Device{
    public PC(int id, String name, Room room) {
        super(id, name, room);
    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }

    public void programming() {
        setPowerConsumed(getPowerConsumed() + 0.1);
    }
}

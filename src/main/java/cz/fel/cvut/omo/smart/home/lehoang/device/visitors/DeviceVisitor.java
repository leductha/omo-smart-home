package cz.fel.cvut.omo.smart.home.lehoang.device.visitors;

import cz.fel.cvut.omo.smart.home.lehoang.device.Blinds;
import cz.fel.cvut.omo.smart.home.lehoang.device.CDPlayer;
import cz.fel.cvut.omo.smart.home.lehoang.device.Cooker;
import cz.fel.cvut.omo.smart.home.lehoang.device.Fridge;
import cz.fel.cvut.omo.smart.home.lehoang.device.Heater;
import cz.fel.cvut.omo.smart.home.lehoang.device.Lights;
import cz.fel.cvut.omo.smart.home.lehoang.device.Microwave;
import cz.fel.cvut.omo.smart.home.lehoang.device.PC;
import cz.fel.cvut.omo.smart.home.lehoang.device.TV;
import cz.fel.cvut.omo.smart.home.lehoang.device.VacuumCleaner;

public interface DeviceVisitor {
    void visit(PC pc, Action action);
    void visit(CDPlayer cdPlayer, Action action);
    void visit(Cooker cooker, Action action);
    void visit(Fridge fridge, Action action);
    void visit(Heater heater, Action action);
    void visit(Microwave microwave, Action action);
    void visit(VacuumCleaner vacuumCleaner, Action action);
    void visit(TV tv, Action action);
    void visit(Blinds blinds, Action action);
    void visit(Lights lights, Action action);
}

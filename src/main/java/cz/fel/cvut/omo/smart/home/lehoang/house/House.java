package cz.fel.cvut.omo.smart.home.lehoang.house;

import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.ActivityAndUsageReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.EventReporter;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.Sensor;
import cz.fel.cvut.omo.smart.home.lehoang.sun.*;
import cz.fel.cvut.omo.smart.home.lehoang.vehicles.Vehicle;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

public class House extends AbstractArea {
    private static final Logger LOG = Logger.getLogger(House.class.getName());

    private final List<Vehicle> vehicles;
    private final int houseId;
    private final List<Floor> floors;

    private EventReporter eventReporter;
    private ActivityAndUsageReporter activityAndUsageReporter;
    private Weather weather;

    public House(String name, int houseId) {
        super(name);
        this.houseId = houseId;
        this.floors = new ArrayList<>();
        this.vehicles = new ArrayList<>();
        this.eventReporter = new EventReporter();
        this.activityAndUsageReporter = new ActivityAndUsageReporter();
        initSun();
    }

    public void addFloor(Floor newFloor) {
        boolean alreadyExists = floors.stream()
                .anyMatch(floor -> floor.getLevel() == newFloor.getLevel() || Objects.equals(floor.getName(), newFloor.getName()));

        if (!alreadyExists) {
            floors.add(newFloor);
        }
    }

    public void addVehicle(Vehicle newVehicle) {
        boolean alreadyExists = vehicles.stream()
                .anyMatch(vehicle -> Objects.equals(vehicle.getName(), newVehicle.getName()));

        if (!alreadyExists) {
            vehicles.add(newVehicle);
        }
    }

    public List<Floor> getFloors() {
        return floors;
    }

    private void initSun() {
        Sun sun = new Sun();
        SunshineAdapter sunshineAdapter = new SunshineAdapter(sun);
        this.weather = new Weather(sunshineAdapter);
        weather.weatherConditions();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Person> getAllPeople() {
        return getFloors()
                .stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getAllPeople().stream())
                .toList();
    }

    @Override
    public List<Pet> getAllPets() {
        return getFloors()
                .stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getAllPets().stream())
                .toList();
    }

    @Override
    public List<Device> getAllDevices() {
        return getFloors()
                .stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getAllDevices().stream())
                .toList();
    }

    @Override
    public List<Sensor> getAllSensors() {
        return getFloors()
                .stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getAllSensors().stream())
                .toList();
    }

    @Override
    public String toString() {
        return "House name " + name + ", id: " + houseId;
    }

    public List<Room> getAllRooms() {
        List<Room> allRooms = new ArrayList<>();
        for (Floor floor : floors) {
            for (Room room : floor.getRooms()) {
                allRooms.add(room);
            }
        }
        return allRooms;
    }

    public List<Vehicle> getAllVehicles() {
        return vehicles;
    }

    public Room findRoomByName(String name) {
        for (Room room : this.getAllRooms()) {
            if (room.getName().equals(name)) {
                return room;
            }
        }
        return null;
    }

    public Person findPersonByName(String name) {
        for (Person person : getAllPeople()) {
            if (person.getName().equals(name)) {
                return person;
            }
        }
        return null;
    }

    public Pet findPetByName(String name) {
        for (Pet pet : getAllPets()) {
            if (pet.getName().equals(name)) {
                return pet;
            }
        }
        return null;
    }

    public Device findDeviceByIdAndName(int id, String name) {
        for (Room room : this.getAllRooms()) {
            for (Device device : room.getAllDevices()) {
                if (device.getDeviceId() == id && device.getName().equals(name)) {
                    return device;
                }
            }
        }
        return null;
    }

    public EventReporter getEventReporter() {
        return this.eventReporter;
    }

    public ActivityAndUsageReporter getActivityAndUsageReporter() {
        return this.activityAndUsageReporter;
    }

    public void getWeatherOutside() {
        LOG.info(weather.getWeatherConditions());
    }
}

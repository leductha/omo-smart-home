package cz.fel.cvut.omo.smart.home.lehoang.device.visitors;

import cz.fel.cvut.omo.smart.home.lehoang.device.Blinds;
import cz.fel.cvut.omo.smart.home.lehoang.device.CDPlayer;
import cz.fel.cvut.omo.smart.home.lehoang.device.Cooker;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.IdleState;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OffState;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OnState;
import cz.fel.cvut.omo.smart.home.lehoang.device.Fridge;
import cz.fel.cvut.omo.smart.home.lehoang.device.Heater;
import cz.fel.cvut.omo.smart.home.lehoang.device.Lights;
import cz.fel.cvut.omo.smart.home.lehoang.device.Microwave;
import cz.fel.cvut.omo.smart.home.lehoang.device.PC;
import cz.fel.cvut.omo.smart.home.lehoang.device.TV;
import cz.fel.cvut.omo.smart.home.lehoang.device.VacuumCleaner;

import java.util.Objects;
import java.util.logging.Logger;

public class UseDeviceVisitor implements DeviceVisitor {
    private static final Logger LOG = Logger.getLogger(UseDeviceVisitor.class.getName());

    @Override
    public void visit(Fridge fridge, Action action) {
            if (fridge.getState() instanceof OnState || fridge.getState() instanceof IdleState) {
                switch (action) {
                    case CONSUME: fridge.consumeFood(); break;
                    case PUTIN: fridge.putFoodInFridge(); break;
                    case TURNON: break;
                    default:
                        LOG.warning("Unsupported action for Fridge");
                }
                fridge.use();
                fridge.setPowerConsumed(fridge.getPowerConsumed() + 0.1);
            } else {
                LOG.info("Fridge is not turned on.");
            }
    }

    @Override
    public void visit(TV tv, Action action) {
        if (action == Action.TURNON) {
            tv.turnOn();
        } else if (action == Action.TURNOFF) {
            tv.turnOff();
        } else {
            if (tv.getState() instanceof OnState || tv.getState() instanceof IdleState) {
                switch (action) {
                    case VOLUMEDOWN: tv.volumeDown(); break;
                    case VOLUMEUP: tv.volumeUp(); break;
                    default:
                        LOG.warning("Unsupported action for TV");
                }
                tv.use();
                tv.setPowerConsumed(tv.getPowerConsumed() + 0.1);
            } else {
                LOG.info("TV is not turned on.");
            }
        }
    }

    @Override
    public void visit(Cooker cooker, Action action) {
        if (action == Action.TURNON) {
            cooker.turnOn();
        } else if (action == Action.TURNOFF) {
            cooker.turnOff();
        } else {
            if (cooker.getState() instanceof OnState || cooker.getState() instanceof IdleState) {
                switch (action) {
                    case HIGHHEAT: cooker.setHeat(200); break;
                    case MEDIUMHEAT: cooker.setHeat(150); break;
                    case LOWHEAT: cooker.setHeat(100); break;
                    default: LOG.warning("Unsupported action for Cooker");
                }
                cooker.use();
            } else {
                LOG.info("Cooker is not turned on.");
            }
        }
        cooker.setPowerConsumed(cooker.getPowerConsumed() + ((double) cooker.getHeat() / 100));
    }


    @Override
    public void visit(PC pc, Action action) {
        if (action == Action.TURNON) {
            pc.turnOn();
        } else if (action == Action.TURNOFF) {
            pc.turnOff();
        } else {
            if (pc.getState() instanceof OnState || pc.getState() instanceof IdleState) {
                if (Objects.requireNonNull(action) == Action.PROGRAMMING) {
                    pc.programming();
                } else {
                    LOG.warning("Unsupported action for PC");
                }
                pc.use();
            } else {
                LOG.info("PC is not turned on.");
            }
        }
    }

    @Override
    public void visit(CDPlayer cdPlayer, Action action) {
        if (action == Action.TURNON) {
            cdPlayer.turnOn();
        } else if (action == Action.TURNOFF) {
            cdPlayer.turnOff();
        } else {
            if (cdPlayer.getState() instanceof OnState || cdPlayer.getState() instanceof IdleState) {
                if (Objects.requireNonNull(action) == Action.PLAYMUSIC) {
                    cdPlayer.playMusic();
                } else {
                    LOG.warning("Unsupported action for CDPlayer");
                }
                cdPlayer.use();
            } else if (cdPlayer.getState() instanceof OffState){
                LOG.info("CDPlayer is not turned on.");
            }
        }
    }

    @Override
    public void visit(Microwave microwave, Action action) {
        if (action == Action.TURNON) {
            microwave.turnOn();
        } else if (action == Action.TURNOFF) {
            microwave.turnOff();
        } else {
            if (microwave.getState() instanceof OnState || microwave.getState() instanceof IdleState) {
                switch (action) {
                    case HIGHHEAT: microwave.heatFood(200); break;
                    case MEDIUMHEAT: microwave.heatFood(150); break;
                    case LOWHEAT: microwave.heatFood(100); break;
                    default:
                        LOG.warning("Unsupported action for Microwave");
                }
                microwave.use();
            } else {
                LOG.info("Microwave is not turned on.");
            }
            microwave.setPowerConsumed(microwave.getPowerConsumed() + ((double) microwave.getTemperature() / 100));
        }
    }

    @Override
    public void visit (VacuumCleaner vacuumCleaner, Action action) {
        if (action == Action.TURNON) {
            vacuumCleaner.turnOn();
        } else if (action == Action.TURNOFF) {
            vacuumCleaner.turnOff();
        } else {
            if (vacuumCleaner.getState() instanceof OnState || vacuumCleaner.getState() instanceof IdleState) {
                vacuumCleaner.use();
            } else if (vacuumCleaner.getState() instanceof IdleState) {
                vacuumCleaner.turnOn();
                vacuumCleaner.use();
            }
            else {
                LOG.info("VacuumCleaner is not turned on.");
            }
            vacuumCleaner.setPowerConsumed(vacuumCleaner.getPowerConsumed() + 1);
        }
    }

    @Override
    public void visit(Heater heater, Action action) {
        switch (action) {
            case TURNON: heater.turnOn(); break;
            case TURNOFF: heater.turnOff(); break;
            default:
                LOG.warning("Unsupported action for Heater");

        }
    }

    @Override
    public void visit(Lights lights, Action action) {
        switch (action) {
            case TURNON: lights.turnOn(); break;
            case TURNOFF: lights.turnOff(); break;
            default:
                LOG.warning("Unsupported action for Lights");

        }
    }

    @Override
    public void visit(Blinds blinds, Action action) {
        switch (action) {
            case TURNON: blinds.turnOn(); break;
            case TURNOFF: blinds.turnOff(); break;
            default:
                LOG.warning("Unsupported action for Blinds");

        }
    }


}

package cz.fel.cvut.omo.smart.home.lehoang.sensor;

import cz.fel.cvut.omo.smart.home.lehoang.device.Observer;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.event.PowerEvent;

import java.util.ArrayList;
import java.util.List;

public class PowerSensor extends Sensor {
    private List<Observer> observers = new ArrayList<>();
    private Room room;

    public PowerSensor(Room room) {
        this.room = room;
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }


    @Override
    public void removeObserver(Observer o) {
        int index = observers.indexOf(o);
        if (index >= 0) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObservers(Event event) {
        for (Observer observer : observers) {
            observer.update(event);
        }
    }

    public void blowFuse() {
        PowerEvent powerEvent = new PowerEvent("Power Event");
        powerEvent.setCreator(this);

        for (Sensor s : room.getFloor().getHouse().getAllSensors()) {
            if (s instanceof PowerSensor){
                s.notifyObservers(powerEvent);
            }
        }

    }

    @Override
    public String toString() {
        return "PowerSensor {" +
                "observers=" + observers +"}";
    }
}

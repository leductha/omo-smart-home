package cz.fel.cvut.omo.smart.home.lehoang.house;

import cz.fel.cvut.omo.smart.home.lehoang.device.DeviceFactory;
import cz.fel.cvut.omo.smart.home.lehoang.exception.JsonFileLoadException;
import cz.fel.cvut.omo.smart.home.lehoang.people.PersonFactory;
import cz.fel.cvut.omo.smart.home.lehoang.pets.PetFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.people.PersonType;
import cz.fel.cvut.omo.smart.home.lehoang.vehicles.Vehicle;
import cz.fel.cvut.omo.smart.home.lehoang.vehicles.VehicleType;

import static cz.fel.cvut.omo.smart.home.lehoang.pets.PetType.CAT;
import static cz.fel.cvut.omo.smart.home.lehoang.pets.PetType.DOG;

public class HouseConfig {
    private static final Logger LOG = Logger.getLogger(HouseConfig.class.getName());

    private String path;

    private ObjectMapper objectMapper;

    private JsonNode jsonFile;

    private House house;

    public HouseConfig(String path) throws JsonFileLoadException {
        this.path = path;
        this.objectMapper = new ObjectMapper();
        try {
            loadJsonFile();
            loadHouse();
            loadFloors();
            loadRooms();
            loadDevices();
            loadPeople();
            loadPets();
            loadVehicles();
        } catch (Exception e) {
            throw new JsonFileLoadException("Error configuring the house: " + e.getMessage());
        }
    }

    private void loadVehicles() {
        JsonNode vehicles = jsonFile.get("Vehicles");

        for (JsonNode vehicle : vehicles) {
            String type = vehicle.get("type").asText();
            String name = vehicle.get("name").asText();

            switch (type) {
                case "MOTORBIKE" : house.addVehicle(new Vehicle(name, VehicleType.MOTORBIKE)); break;
                case "BICYCLE" : house.addVehicle(new Vehicle(name, VehicleType.BICYCLE)); break;
                case "CAR" : house.addVehicle(new Vehicle(name, VehicleType.CAR)); break;
                case "SKI" : house.addVehicle(new Vehicle(name, VehicleType.SKI)); break;
            }
        }
    }

    private void loadPets() {
        PetFactory petFactory = new PetFactory();
        JsonNode pets = jsonFile.get("Pets");

        if (pets == null) {
            return;
        }

        for (JsonNode pet : pets) {
            String type = pet.get("type").asText();
            String name = pet.get("name").asText();
            int age = pet.get("age").asInt();
            Room room = getRoomByName(pet.get("room").asText());

            switch (type) {
                case "DOG" -> petFactory.createPet(DOG, name, age, room);
                case "CAT" -> petFactory.createPet(CAT, name, age, room);
                default -> LOG.warning("Not supported");
            }
        }
    }

    private void loadPeople() {
        PersonFactory personFactory = new PersonFactory();
        JsonNode people = jsonFile.get("People");

        if (people == null) {
            return;
        }

        for (JsonNode person : people) {
            String type = person.get("type").asText();
            String name = person.get("name").asText();
            int age = person.get("age").asInt();
            Room room = getRoomByName(person.get("room").asText());

            switch (type) {
                case "DAD" -> personFactory.createPerson(PersonType.DAD, name, age, room);
                case "MUM" -> personFactory.createPerson(PersonType.MUM, name, age, room);
                case "SON" -> personFactory.createPerson(PersonType.SON, name, age, room);
                case "DAUGHTER" -> personFactory.createPerson(PersonType.DAUGHTER, name, age, room);
                default -> LOG.warning("WRONG");
            }
        }
    }

    public void loadJsonFile() throws JsonFileLoadException {
        try {
            File houseJson = new File(path);
            jsonFile = objectMapper.readTree(houseJson);
        } catch (IOException e) {
            throw new JsonFileLoadException("Error loading JSON file: " + e.getMessage());
        }
    }

    public void loadHouse() {
        JsonNode building = jsonFile.get("House");
        String houseName = building.get("name").asText();
        int houseId = building.get("houseId").asInt();

        this.house = new House(houseName, houseId);
    }

    public void loadFloors() {
        JsonNode floors = jsonFile.get("Floors");
        FloorBuilder floorBuilder = new FloorBuilder();

        if (floors == null) {
            return;
        }

        for (JsonNode floorNode : floors) {
            String name = floorNode.get("name").asText();
            int level = floorNode.get("level").asInt();

            Floor floor = floorBuilder.setHouse(house).setName(name).setLevel(level).build();
            this.house.addFloor(floor);
        }
    }

    public void loadRooms() {
        JsonNode rooms = jsonFile.get("Rooms");

        if (rooms == null) {
            return;
        }

        for (JsonNode roomNode : rooms) {
            String name = roomNode.get("name").asText();
            int floorNumber = roomNode.get("floor").asInt();
            boolean hasWindows = roomNode.get("hasWindows").asBoolean();

            Floor selectedFloor = this.house.getFloors().stream()
                    .filter(floor -> floor.getLevel() == floorNumber)
                    .toList()
                    .get(0);

            Room room = new Room(name, selectedFloor, hasWindows);
            selectedFloor.addRoom(room);
        }
    }

    public void loadDevices() {
        JsonNode devices = jsonFile.get("Devices");
        DeviceFactory deviceFactory = new DeviceFactory();

        if (devices == null) {
            return;
        }

        for (JsonNode deviceNode : devices) {
            int id = deviceNode.get("id").asInt();
            String name = deviceNode.get("name").asText();
            String roomName = deviceNode.get("room").asText();
            Room room = getRoomByName(roomName);

            Device device = deviceFactory.createDevice(id, name, room);
            room.addDevice(device);
        }


    }

    public Room getRoomByName(String roomName) {
        return this.house.getFloors().stream()
                .flatMap(floor -> floor.getRooms().stream())
                .filter(room -> Objects.equals(room.getName(), roomName))
                .findFirst()
                .orElse(null);
    }

    public House getHouse() {
        LOG.log(Level.INFO, "House built");
        return this.house;
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.pets;

import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class Dog extends Pet {


    public Dog(String name, int age, Room room) {
        super(name, age, room);
    }

    @Override
    public void makeSound() {
        System.out.println("bark");
    }


}

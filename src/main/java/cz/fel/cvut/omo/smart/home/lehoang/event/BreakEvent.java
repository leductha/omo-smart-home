package cz.fel.cvut.omo.smart.home.lehoang.event;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;

public class BreakEvent extends Event{
    private Device device;

    public BreakEvent(String name) {
        super(name);
    }

    public BreakEvent (BreakEvent breakEvent) {
        super(breakEvent);
    }

    @Override
    public BreakEvent clone() {
        return new BreakEvent(this);
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}

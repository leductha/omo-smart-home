package cz.fel.cvut.omo.smart.home.lehoang.exception;

public class JsonFileLoadException extends Exception {
    public JsonFileLoadException(String message) {
        super(message);
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.device.visitors;

public interface Visitor {
    void accept(DeviceVisitor deviceVisitor, Action action);
}

package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class Microwave extends Device{
    private int temperature;
    public Microwave(int id, String name, Room room) {
        super(id, name, room);
    }

    public void heatFood (int temperature) {
        this.temperature = temperature;
    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }

    public int getTemperature() {
        return temperature;
    }
}

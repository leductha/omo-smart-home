package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.TemperatureSensor;

public class DeviceFactory {

    public Device createDevice(int id, String name, Room room) {
        return switch (name) {
            case "PC" -> createPC(id, name, room);
            case "CDPlayer" -> createCDPlayer(id, name, room);
            case "Cooker" -> createCooker(id, name, room);
            case "Fridge" -> createFridge(id, name, room);
            case "Heater" -> createHeater(id, name, room);
            case "Microwave" -> createMicrowave(id, name, room);
            case "VacuumCleaner" -> createVacuumCleaner(id, name, room);
            case "TV" -> createTV(id, name, room);
            default -> throw new IllegalArgumentException("Unknown device type: " + name);
        };
    }

    private Microwave createMicrowave(int id, String name, Room room) {
        Microwave microwave = new Microwave(id, name, room);
        room.getPowerSensor().registerObserver(microwave);
        room.getMoveSensor().registerObserver(microwave);
        return microwave;
    }

    private VacuumCleaner createVacuumCleaner (int id, String name, Room room) {
        VacuumCleaner cleaner = new VacuumCleaner(id, name, room);
        room.getPowerSensor().registerObserver(cleaner);
        room.getMoveSensor().registerObserver(cleaner);
        return cleaner;
    }

    private Heater createHeater(int id, String name, Room room) {
        if (!room.hasSensor(TemperatureSensor.class)) {
            room.addSensor(new TemperatureSensor(25, room));
        }

        Heater newHeater = new Heater(id, name, room);
        room.getTemperatureSensor().registerObserver(newHeater);
        room.getPowerSensor().registerObserver(newHeater);
        return newHeater;
    }

    private Cooker createCooker(int id, String name, Room room) {
        Cooker cooker = new Cooker(id, name, room);
        room.getPowerSensor().registerObserver(cooker);
        room.getMoveSensor().registerObserver(cooker);
        return cooker;
    }

    private CDPlayer createCDPlayer(int id, String name, Room room) {
        CDPlayer cdPlayer = new CDPlayer(id, name, room);
        room.getPowerSensor().registerObserver(cdPlayer);
        room.getMoveSensor().registerObserver(cdPlayer);
        return cdPlayer;
    }

    private PC createPC (int id, String name, Room room) {
        PC pc = new PC(id, name, room);
        room.getPowerSensor().registerObserver(pc);
        room.getMoveSensor().registerObserver(pc);
        return pc;
    }

    public Fridge createFridge(int id, String name, Room room) {
        Fridge fridge = new Fridge(id, name, room);
        room.getPowerSensor().registerObserver(fridge);
        room.getMoveSensor().registerObserver(fridge);
        return fridge;
    }

    public TV createTV(int id, String name, Room room) {
        TV tv = new TV(id, name, room);
        room.getPowerSensor().registerObserver(tv);
        room.getMoveSensor().registerObserver(tv);
        return tv;
    }



}

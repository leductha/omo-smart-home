package cz.fel.cvut.omo.smart.home.lehoang.device.devicestate;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;

public abstract class DeviceState {
    protected Device device;

    protected DeviceState(Device device) {
        this.device = device;
    }

    public abstract void breakDevice();
    public abstract void turnOn();
    public abstract void turnOff();
    public abstract void turnIdle();


}

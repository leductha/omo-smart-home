package cz.fel.cvut.omo.smart.home.lehoang.people;

import cz.fel.cvut.omo.smart.home.lehoang.device.CDPlayer;
import cz.fel.cvut.omo.smart.home.lehoang.device.Cooker;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.device.Fridge;
import cz.fel.cvut.omo.smart.home.lehoang.device.Microwave;
import cz.fel.cvut.omo.smart.home.lehoang.device.Observer;
import cz.fel.cvut.omo.smart.home.lehoang.device.PC;
import cz.fel.cvut.omo.smart.home.lehoang.device.TV;
import cz.fel.cvut.omo.smart.home.lehoang.device.VacuumCleaner;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.UseDeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.event.HungerEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.BreakEvent;
import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.ActivityAndUsageReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.EventReporter;
import cz.fel.cvut.omo.smart.home.lehoang.vehicles.Vehicle;
import cz.fel.cvut.omo.smart.home.lehoang.vehicles.VehicleType;

import java.util.logging.Logger;

import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.CLEAN;
import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.CONSUME;
import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.PLAYMUSIC;
import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.PROGRAMMING;
import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.PUTIN;
import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.TURNOFF;
import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.TURNON;
import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.VOLUMEDOWN;
import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.VOLUMEUP;

public class Person implements DeviceInteractions, Observer {
    private static final Logger LOG = Logger.getLogger(Person.class.getName());

    private String name;
    private int age;
    protected Room room;
    private Vehicle vehicle;
    private EventReporter eventReporter;
    private ActivityAndUsageReporter activityAndUsageReporter;

    public Person(String name, int age, Room room) {
        this.name = name;
        this.age = age;
        this.room = room;
        this.eventReporter = room.getHouse().getEventReporter();
        this.activityAndUsageReporter = room.getHouse().getActivityAndUsageReporter();
        this.vehicle = null;
    }

    public void moveTo (Room roomToGo) {
        if (this.getRoom() == roomToGo) {
            return;
        }
        activityAndUsageReporter.addActivity(name + " goes to " + roomToGo.getName());


        if (this.vehicle != null) {
            stop();
        }

        if (this.room != null) {
            this.room.removePerson(this);
            this.room.getMoveSensor().notifyMove();
        }

        roomToGo.addPerson(this);
        roomToGo.getMoveSensor().notifyMove();
        this.room = roomToGo;

    }

    public void goOutside () {
        if (this.room != null) {
            this.room.removePerson(this);
            this.room = null;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Room getRoom() {
        return this.room;
    }

    @Override
    public void turnOn(Device device) {
        if (this.getRoom() != device.getRoom()) {
            this.moveTo(device.getRoom());
        }
        device.turnOn();
        device.accept(new UseDeviceVisitor(), TURNON);
        activityAndUsageReporter.addActivity(this.getName() + " turns ON the " + device);

    }

    @Override
    public void turnOff(Device device) {
        if (this.getRoom() != device.getRoom()) {
            this.moveTo(device.getRoom());
        }
        device.turnOff();
        device.accept(new UseDeviceVisitor(), TURNOFF);
        activityAndUsageReporter.addActivity(this.getName() + " turns OFF the " + device);
    }

    @Override
    public void cook (Cooker cooker, Action action) {
        if (this.getRoom() != cooker.getRoom()) {
            this.moveTo(cooker.getRoom());
        }
        cooker.accept(new UseDeviceVisitor(), action);
        activityAndUsageReporter.addActivity(this.getName() + " cooks on " + cooker);
    }

    @Override
    public void turnVolumeDown(TV tv) {
        if (this.getRoom() != tv.getRoom()) {
            this.moveTo(tv.getRoom());
        }
        tv.accept(new UseDeviceVisitor(), VOLUMEDOWN);
        activityAndUsageReporter.addActivity(this.getName() + " turns volume down on " + tv);

    }

    @Override
    public void turnVolumeUp(TV tv) {
        if (this.getRoom() != tv.getRoom()) {
            this.moveTo(tv.getRoom());
        }
        tv.accept(new UseDeviceVisitor(), VOLUMEUP);
        activityAndUsageReporter.addActivity(this.getName() + " turns volume up on " + tv);

    }

    @Override
    public void code(PC pc) {
        if (this.getRoom() != pc.getRoom()) {
            this.moveTo(pc.getRoom());
        }
        pc.accept(new UseDeviceVisitor(), PROGRAMMING);
        activityAndUsageReporter.addActivity(this.getName() + " codes on " + pc);
    }

    @Override
    public void consumeFood(Fridge fridge) {
        if (this.getRoom() != fridge.getRoom()) {
            this.moveTo(fridge.getRoom());
        }
        fridge.accept(new UseDeviceVisitor(), CONSUME);
        activityAndUsageReporter.addActivity(this.getName() + " consumes food from " + fridge);
    }

    @Override
    public void putFood(Fridge fridge) {
        if (this.getRoom() != fridge.getRoom()) {
            this.moveTo(fridge.getRoom());
        }
        fridge.accept(new UseDeviceVisitor(), PUTIN);
        activityAndUsageReporter.addActivity(this.getName() + " puts food into " + fridge);

    }

    @Override
    public void heatFood (Microwave microwave, Action action) {
        if (this.getRoom() != microwave.getRoom()) {
            this.moveTo(microwave.getRoom());
        }

        microwave.accept(new UseDeviceVisitor(), action);
        activityAndUsageReporter.addActivity(this.getName() + " heats food in " + microwave);

    }

    @Override
    public void clean(VacuumCleaner vacuumCleaner, Room room) {
        vacuumCleaner.accept(new UseDeviceVisitor(), CLEAN);
        vacuumCleaner.clean(this, room);
        activityAndUsageReporter.addActivity(this.getName() + " cleans the room " + room.getName() + " with " + vacuumCleaner);
    }

    @Override
    public void listen(CDPlayer cdPlayer) {
        cdPlayer.accept(new UseDeviceVisitor(), PLAYMUSIC);
        activityAndUsageReporter.addActivity(this.getName() + " plays music using " + cdPlayer);
    }

    public void ride(Vehicle vehicle) {
        if (vehicle.isInUse()) {
            return;
        }

        if ((vehicle.getType() == VehicleType.MOTORBIKE || vehicle.getType() == VehicleType.CAR) && this.getAge() < 18) {
            LOG.warning(this.getName() + " has no driving license");
            return;
        }

        activityAndUsageReporter.addActivity(this.getName() + " is using " + vehicle.getName());
        goOutside();
        vehicle.setInUse(true);
        this.vehicle = vehicle;
    }


    public Vehicle getVehicle() {
        return vehicle;
    }

    public void stop() {
        this.vehicle.setInUse(false);
        this.vehicle = null;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " Name: " + this.name + ", Age: " + this.age + ", Room: " + this.room.getName();
    }

    @Override
    public void update(Event event) {
        if (event instanceof HungerEvent) {
            Pet pet = (Pet)event.getCreator();
            event.setReactor(this);
            eventReporter.add(event);

            if (this.vehicle != null) {
                this.stop();
            }

            this.moveTo(pet.getRoom());
            activityAndUsageReporter.addActivity(this.name + " feeds " + ((Pet) event.getCreator()).getName());
        }
        else if (event instanceof BreakEvent) {
            Device device = ((BreakEvent) event).getDevice();
            event.setReactor(this);
            eventReporter.add(event);

            if (this.vehicle != null) {
                this.stop();
            }

            this.moveTo(device.getRoom());
            device.turnOn();
            activityAndUsageReporter.addActivity(this.name + " repairs " + ((BreakEvent) event).getDevice().getName());
        }
    }
}

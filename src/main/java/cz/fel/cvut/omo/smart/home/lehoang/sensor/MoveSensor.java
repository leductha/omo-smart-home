package cz.fel.cvut.omo.smart.home.lehoang.sensor;

import cz.fel.cvut.omo.smart.home.lehoang.device.Observer;
import cz.fel.cvut.omo.smart.home.lehoang.event.MoveEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

import java.util.ArrayList;
import java.util.List;

public class MoveSensor extends Sensor{
    private List<Observer> observers = new ArrayList<>();
    private Room room;


    public MoveSensor(Room room) {
        this.room = room;
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }


    @Override
    public void removeObserver(Observer o) {
        int index = observers.indexOf(o);
        if (index >= 0) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObservers(Event event) {
        for (Observer observer : observers) {
            observer.update(event);
        }
    }

    public void notifyMove() {
        MoveEvent moveEvent = new MoveEvent("Move Event");
        moveEvent.setCreator(this);

        notifyObservers(moveEvent);


    }
}

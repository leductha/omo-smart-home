package cz.fel.cvut.omo.smart.home.lehoang.event;


public class PowerEvent extends Event{

    public PowerEvent(String name) {
        super(name);
    }

    public PowerEvent(PowerEvent powerEvent) {
        super(powerEvent);
    }

    @Override
    public PowerEvent clone() {
        return new PowerEvent(this);
    }

    @Override
    public String toString() {
        return getName() + ", Reactor: " + getReactor() + ", Creator: " + getCreator();
    }
}

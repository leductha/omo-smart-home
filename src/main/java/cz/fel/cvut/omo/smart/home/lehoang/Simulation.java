package cz.fel.cvut.omo.smart.home.lehoang;

import cz.fel.cvut.omo.smart.home.lehoang.device.Blinds;
import cz.fel.cvut.omo.smart.home.lehoang.device.CDPlayer;
import cz.fel.cvut.omo.smart.home.lehoang.device.Cooker;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.device.Fridge;
import cz.fel.cvut.omo.smart.home.lehoang.device.Heater;
import cz.fel.cvut.omo.smart.home.lehoang.device.Lights;
import cz.fel.cvut.omo.smart.home.lehoang.device.Microwave;
import cz.fel.cvut.omo.smart.home.lehoang.device.PC;
import cz.fel.cvut.omo.smart.home.lehoang.device.TV;
import cz.fel.cvut.omo.smart.home.lehoang.device.VacuumCleaner;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OffState;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.house.HouseConfig;
import cz.fel.cvut.omo.smart.home.lehoang.house.House;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;
import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.ActivityAndUsageReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.ConsumptionReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.EventReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.HouseConfigurationReporter;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.*;
import cz.fel.cvut.omo.smart.home.lehoang.vehicles.Vehicle;
import cz.fel.cvut.omo.smart.home.lehoang.vehicles.VehicleType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;


public class Simulation {
    private static final Logger LOG = Logger.getLogger(Simulation.class.getName());
    private Random random = new Random();
    private House house;

    private static final String PATH = "src/main/resources/house.json";

    private HouseConfigurationReporter houseConfigurationReporter;
    private ConsumptionReporter consumptionReporter;
    private ActivityAndUsageReporter activityAndUsageReporter;
    private EventReporter eventReporter;

    private List<Room> rooms = new ArrayList<>();
    private List<Device> devices = new ArrayList<>();
    private List<Person> people = new ArrayList<>();
    private List<Pet> pets = new ArrayList<>();
    private List<Sensor> sensors = new ArrayList<>();

    public Simulation() {
        load();
    }

    public void load() {
        try {
            HouseConfig houseConfig = new HouseConfig(PATH);
            this.house = houseConfig.getHouse();

            this.rooms = house.getAllRooms();
            this.people = house.getAllPeople();
            this.pets = house.getAllPets();

            this.houseConfigurationReporter = new HouseConfigurationReporter();
            this.activityAndUsageReporter = new ActivityAndUsageReporter();
            this.consumptionReporter = new ConsumptionReporter();
            this.eventReporter = house.getEventReporter();

            addDevices();
            addSensors();
            System.out.println("--- \uD83C\uDFE1[HOUSE SUCCESSFULLY CONFIGURED]\uD83C\uDFE1 ---");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void addSensors() {
        for (Sensor sensor : house.getAllSensors()) {
            if (sensor instanceof TemperatureSensor || sensor instanceof WindSensor) {
                sensors.add(sensor);
            }
        }
    }

    private void addDevices() {
        for (Device device : house.getAllDevices()) {
            if (!(device instanceof Lights || device instanceof Blinds)) {
                devices.add(device);
            }
        }
    }


    public void simulate(int hours) {
        int timeIntervalMillis = 1000;

        for (int hour = 0; hour < hours; hour++) {
            int randomNumber = random.nextInt(1,5);
            System.out.println("--- Simulation Time: Hour " + (hour + 1) + " ---");

            generateActivity();
            if (randomNumber % 2 == 0) {generateEvent();}
            if (randomNumber == 4) {generateRide();}

            try {
                Thread.sleep(timeIntervalMillis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("--- \uD83D\uDCB8[SIMULATION FINISHED SUCCESSFULLY]\uD83D\uDCB8 ---");
    }

    private void generateRide() {
        List<Vehicle> vehiclesForKids = house.getAllVehicles().stream()
                .filter(vehicle -> vehicle.getType() == VehicleType.BICYCLE || vehicle.getType() == VehicleType.SKI)
                .toList();

        Person randomPerson = people.get(random.nextInt(people.size()));
        if (randomPerson.getAge() < 18) {
            randomPerson.ride(vehiclesForKids.get(random.nextInt(vehiclesForKids.size())));
        } else {
            randomPerson.ride(house.getAllVehicles().get(random.nextInt(house.getAllVehicles().size())));
        }

    }


    public void generateActivity() {
        int num = random.nextInt(1,4);

        Room randomRoom = rooms.get(random.nextInt(rooms.size()));
        Device randomDevice = devices.get(random.nextInt(devices.size()));
        Person randomPerson = people.get(random.nextInt(people.size()));

        if (randomDevice instanceof Heater) {
            Heater heater = (Heater) randomDevice;
            randomPerson.turnOn(heater);
        }
        else if (randomDevice instanceof PC) {
            PC pc = (PC) randomDevice;

            if (pc.getState() instanceof OffState) {
                randomPerson.turnOn(pc);
            }

            randomPerson.code(pc);
        }
        else if (randomDevice instanceof TV) {
            TV tv = (TV) randomDevice;

            if (tv.getState() instanceof OffState) {
                randomPerson.turnOn(tv);
            }

            if (random.nextBoolean()) {
                randomPerson.turnVolumeUp(tv);
            } else {
                randomPerson.turnVolumeDown(tv);
            }
        }
        else if (randomDevice instanceof Fridge) {
            Fridge fridge = (Fridge) randomDevice;
            if (fridge.getState() instanceof OffState){
                randomPerson.turnOn(fridge);
            }
            if (random.nextBoolean()) {
                randomPerson.consumeFood(fridge);
            } else {
                randomPerson.putFood(fridge);
            }
        }
        else if (randomDevice instanceof VacuumCleaner) {
            VacuumCleaner vacuumCleaner = (VacuumCleaner) randomDevice;
            if (vacuumCleaner.getState() instanceof OffState) {
                randomPerson.turnOn(vacuumCleaner);
            }
            randomPerson.clean(vacuumCleaner, randomRoom);
        }
        else if (randomDevice instanceof Microwave) {
            Microwave microwave = (Microwave) randomDevice;
            if (microwave.getState() instanceof OffState) {
                randomPerson.turnOn(microwave);
            }

            switch (num) {
                case 1 -> randomPerson.heatFood(microwave, Action.LOWHEAT);
                case 2 -> randomPerson.heatFood(microwave, Action.MEDIUMHEAT);
                case 3 -> randomPerson.heatFood(microwave, Action.HIGHHEAT);
            }
        }
        else if (randomDevice instanceof CDPlayer) {
            CDPlayer cdPlayer = (CDPlayer) randomDevice;
            if (cdPlayer.getState() instanceof OffState) {
                randomPerson.turnOn(cdPlayer);
            }
            randomPerson.listen(cdPlayer);
        }
        else if (randomDevice instanceof Cooker) {
            Cooker cooker = (Cooker) randomDevice;
            if (cooker.getState() instanceof OffState) {
                randomPerson.turnOn(cooker);
            }

            switch (num) {
                case 1 -> randomPerson.cook(cooker, Action.LOWHEAT);
                case 2 -> randomPerson.cook(cooker, Action.MEDIUMHEAT);
                case 3 -> randomPerson.cook(cooker, Action.HIGHHEAT);
            }
        }
        else {
            System.out.println("Unhandled device type: " + randomDevice.getClass().getSimpleName());
        }
    }

    public void generateEvent() {
        PowerSensor powerSensor = house.getAllRooms().get(0).getPowerSensor();
        int randNum = random.nextInt(1,6);

        if (randNum == 1) {
            powerSensor.blowFuse();
        } else {
            Sensor randomSensor = sensors.get(random.nextInt(sensors.size()));
            if (randomSensor instanceof WindSensor windSensor) {
                windSensor.generateEvent();
            } else {
                TemperatureSensor temperatureSensor = (TemperatureSensor) randomSensor;
                temperatureSensor.generateEvent();
            }
        }
    }

    public void generateReports() {
        houseConfigurationReporter.create(house);
        consumptionReporter.create(house);
        eventReporter.create(house);
        activityAndUsageReporter.create(house);
    }


}

package cz.fel.cvut.omo.smart.home.lehoang.house;
import cz.fel.cvut.omo.smart.home.lehoang.sun.SunshineAdapter;

public class Weather {
    private String weatherConditions;
    private SunshineAdapter sunraysToTempConvert;

    public Weather(SunshineAdapter sunraysToTempConvert) {
        this.sunraysToTempConvert = sunraysToTempConvert;
    }

    public void weatherConditions() {
        float temperature = sunraysToTempConvert.convertToTemperature();

        if (temperature > 25) {
            weatherConditions = "Hot and Sunny weather ☀\uFE0F";
        } else if (temperature < 5) {
            weatherConditions = "Cold and Snowy weather ❄\uFE0F";
        } else {
            weatherConditions = "Mild weather ☁\uFE0F";
        }
    }

    public String getWeatherConditions() {
        return weatherConditions;
    }
}

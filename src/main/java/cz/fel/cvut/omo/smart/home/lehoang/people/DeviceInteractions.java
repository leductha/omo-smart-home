package cz.fel.cvut.omo.smart.home.lehoang.people;

import cz.fel.cvut.omo.smart.home.lehoang.device.CDPlayer;
import cz.fel.cvut.omo.smart.home.lehoang.device.Cooker;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.device.Fridge;
import cz.fel.cvut.omo.smart.home.lehoang.device.Microwave;
import cz.fel.cvut.omo.smart.home.lehoang.device.PC;
import cz.fel.cvut.omo.smart.home.lehoang.device.TV;
import cz.fel.cvut.omo.smart.home.lehoang.device.VacuumCleaner;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public interface DeviceInteractions {
    void turnOn (Device device);
    void turnOff (Device device);
    void cook (Cooker cooker, Action action);
    void turnVolumeUp (TV tv);
    void turnVolumeDown (TV tv);

    void code (PC pc);

    void consumeFood (Fridge fridge);

    void putFood (Fridge fridge);
    void heatFood (Microwave microwave, Action action);
    void clean (VacuumCleaner vacuumCleaner, Room room);

    void listen (CDPlayer cdPlayer);


}

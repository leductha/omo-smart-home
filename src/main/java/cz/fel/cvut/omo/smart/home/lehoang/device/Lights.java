package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OnState;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.event.MoveEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.PowerEvent;

public class Lights extends Device{
    public Lights(int id, String name, Room room) {
        super(id, name, room);
    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }

    @Override
    public void update(Event e) {
        if (e instanceof PowerEvent && this.getState() instanceof OnState) {
            turnOff();

            e.setReactor(this);
            getEventReporter().add(e);

            getActivityAndUsageReporter().addActivity(this.getName() + this.getDeviceId() + " were turned OFF due to blow fuses");
        }
        else if (e instanceof MoveEvent) {
            if (this.getRoom().getAllPeople().isEmpty()) {
                e.setReactor(this);
                turnOff();
                getActivityAndUsageReporter().addActivity(this.getName() + this.getDeviceId() + " in " + this.getRoom() + " were turned OFF, because someone left the room.");
            } else if(!(this.getRoom().getAllPeople().isEmpty()) && this.getState() instanceof OnState){
                // DO NOTHING
            }
            else if(!(this.getRoom().getAllPeople().isEmpty())) {
                e.setReactor(this);
                turnOn();
                setPowerConsumed(getPowerConsumed() + 0.1);
                use();
                getActivityAndUsageReporter().addActivity(this.getName() + this.getDeviceId() + " in " + this.getRoom() + " were turned ON, because someone entered the room.");
            }
        }
    }
}

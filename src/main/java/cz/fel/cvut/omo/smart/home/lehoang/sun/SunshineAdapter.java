package cz.fel.cvut.omo.smart.home.lehoang.sun;

public class SunshineAdapter implements SunshineConverter {

    private Sun sun;

    public SunshineAdapter(Sun sun) {
        this.sun = sun;
    }

    @Override
    public int convertToTemperature() {
        int temp = sun.getSunrays() / 10;
        return temp;
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.reporters;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.house.House;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ActivityAndUsageReporter {
    private static final Logger LOG = Logger.getLogger(ActivityAndUsageReporter.class.getName());

    private static final String FOLDER_PATH = "src/main/java/cz/fel/cvut/omo/smart/home/lehoang/reporters/reports";

    private static final String FILE_NAME = "activityUsage.txt";

    private static final String PATH = FOLDER_PATH + "/" + FILE_NAME;

    private List<String> activities = new ArrayList<>();

    public void addActivity(String activity) {
        String newRecord = activity;
        activities.add(newRecord);
    }

    public void create (House house) {
        try {
            File myObj = new File(PATH);
            if (myObj.createNewFile()) {

                LOG.info("File created: " + myObj.getName());
            }
        } catch (
                IOException e) {
            LOG.info("An error occurred.");
            e.printStackTrace();
        }


        try (FileWriter fw = new FileWriter(PATH)) {

            fw.write("+------------+ \n");
            fw.write("| ACTIVITIES | \n");
            fw.write("+------------+ \n");

            for (String activity : house.getActivityAndUsageReporter().getActivities()) {
                fw.write(activity + "\n");
            }


            fw.write("\n");

            fw.write("+-------+ \n");
            fw.write("| USAGE | \n");
            fw.write("+-------+ \n");

            for (Device device : house.getAllDevices()) {
                fw.write(device.getName() + device.getDeviceId() + ": " + device.getTimesUsed() + "\n");
            }

        } catch (IOException e) {
            LOG.info("Failed writing into file " + FILE_NAME);
            e.printStackTrace();
        }
    }

    public List<String> getActivities() {
        return activities;
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.event;

public class WindEvent extends Event {
    private int wind;

    public WindEvent(String name, int wind) {
        super(name);
        this.wind = wind;
    }

    public WindEvent(WindEvent windEvent) {
        super(windEvent);
        this.wind = windEvent.wind;
    }

    public int getWind() {
        return wind;
    }

    @Override
    public WindEvent clone() {
        return new WindEvent(this);
    }

    @Override
    public String toString() {
        return getName() + ", Reactor: " + getReactor() + ", " + getDescription() + ", Wind: " + wind;
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.sensor;

import cz.fel.cvut.omo.smart.home.lehoang.device.Observer;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.event.BreakEvent;

import java.util.ArrayList;
import java.util.List;

public class BreakSensor extends Sensor{
    private List<Observer> observers = new ArrayList<>(); // ONLY DADS
    private Room room;


    public BreakSensor(Room room) {
        this.room = room;
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }


    @Override
    public void removeObserver(Observer o) {
        int index = observers.indexOf(o);
        if (index >= 0) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObservers(Event event) {
        for (Observer observer : observers) {
            observer.update(event);
        }
    }

    public void notifyBroken(Device device) {
        BreakEvent breakEvent = new BreakEvent("Device broken");
        breakEvent.setDevice(device);
        breakEvent.setCreator(this);

        notifyObservers(breakEvent);
    }

    @Override
    public String toString() {
        return "BreakSensor{" +
                ", observers=" + observers +
                ", room=" + room +
                '}';
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.event.Event;

public interface Observer {
    public void update(Event event);
}

package cz.fel.cvut.omo.smart.home.lehoang.reporters;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.house.House;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

public class ConsumptionReporter {
    private static final Logger LOG = Logger.getLogger(ConsumptionReporter.class.getName());

    private static final String FOLDER_PATH = "src/main/java/cz/fel/cvut/omo/smart/home/lehoang/reporters/reports";

    private static final String FILE_NAME = "consumption.txt";

    private static final String PATH = FOLDER_PATH +"/"+ FILE_NAME;

    private static final int PRICEKWHPERHOUR = 4;


    public void create (House house) {
        try {
            File myObj = new File(PATH);
            if (myObj.createNewFile()) {
                LOG.info("File created: " + myObj.getName());
            }
        } catch (IOException e) {
            LOG.info("An error occurred.");
            e.printStackTrace();
        }


        try (FileWriter fw = new FileWriter(PATH)) {
            double sum = 0;

            fw.write("+---------------------------+ \n");
            fw.write("| CONSUMPTION REPORT in kWh | \n");
            fw.write("+---------------------------+ \n");

            for (Device device : house.getAllDevices()) {
                fw.write(device.getName() + device.getDeviceId() + ": " + String.format("%.2f", device.getPowerConsumed()) + "\n");
                sum += device.getPowerConsumed();
            }
            fw.write("---------------------- \n");

            fw.write("Total cost: " + String.format("%.2f", sum * PRICEKWHPERHOUR) + "Kč");

        } catch (IOException e) {
            LOG.info("Failed writing into file " + FILE_NAME);
            e.printStackTrace();
        }
    }
}

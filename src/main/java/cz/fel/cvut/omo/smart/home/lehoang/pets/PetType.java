package cz.fel.cvut.omo.smart.home.lehoang.pets;

public enum PetType {
    DOG,
    CAT
}

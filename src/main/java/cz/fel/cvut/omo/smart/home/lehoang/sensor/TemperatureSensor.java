package cz.fel.cvut.omo.smart.home.lehoang.sensor;

import cz.fel.cvut.omo.smart.home.lehoang.device.Observer;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.event.TemperatureEvent;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

import java.util.ArrayList;
import java.util.List;


public class TemperatureSensor extends Sensor {
    private float temperatureInside;
    private List<Observer> observers = new ArrayList<>();
    private Room room;

    public TemperatureSensor(float temperatureInside, Room room) {
        this.temperatureInside = temperatureInside;
        this.room = room;
    }
    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int index = observers.indexOf(o);
        if (index >= 0) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObservers(Event event) {
        for (Observer observer : observers) {
            observer.update(event);
        }
    }

    public float getTemperatureInside() {
        return temperatureInside;
    }

    public void setTemperatureInside(float temperatureInside) {
        this.temperatureInside = temperatureInside;
    }

    @Override
    public List<Observer> getObservers() {
        return observers;
    }

    public void setObservers(List<Observer> observers) {
        this.observers = observers;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Event generateEvent() {
        float randomTemperature = (float) Math.random() * 50;
        TemperatureEvent newEvent = new TemperatureEvent("Temperature Event", room, (int) randomTemperature);
        newEvent.setCreator(this);
        notifyObservers(newEvent);
        return newEvent;
    }

    @Override
    public String toString() {
        return "TemperatureSensor{" +
                "temperatureInside=" + temperatureInside +
                ", observers=" + observers +
                ", room=" + room +
                '}';
    }

}

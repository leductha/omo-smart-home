package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

import java.util.logging.Logger;

public class TV extends Device{
    private static final Logger LOG = Logger.getLogger(TV.class.getName());

    private int volume;

    public TV (int id, String name, Room room) {
        super(id, name, room);
        this.volume = 50;
    }

    public void volumeUp() {
        if (volume <= 100) {
            volume++;
        } else {
            LOG.warning("Volume cannot be turned UP");
        }
    }

    public void volumeDown() {
        if (volume >= 0) {
            volume --;
        } else {
            LOG.warning("Volume cannot be turned DOWN");
        }
    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }

    public int getVolume() {
        return volume;
    }
}

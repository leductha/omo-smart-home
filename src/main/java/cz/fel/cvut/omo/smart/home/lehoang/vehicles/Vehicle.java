package cz.fel.cvut.omo.smart.home.lehoang.vehicles;

public class Vehicle {
    private String name;
    private VehicleType type;
    private boolean inUse = false;

    public Vehicle(String name, VehicleType type) {
        this.name = name;
        this.type = type;
    }

    public boolean isInUse() {
        return inUse;
    }

    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return name + " " + getType();
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.event;

import cz.fel.cvut.omo.smart.home.lehoang.device.Observer;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.Subject;

public abstract class Event implements Prototype {
    private String name;
    private Subject creator;
    private Observer reactor;
    private String description;
    protected Room room;

    protected Event(String name) {
        this.name = name;
    }

    protected Event (Event event) {
        this.name = event.name;
        this.creator = event.creator;
        this.reactor = event.reactor;
        this.description = event.description;
        this.room = event.room;
    }

    public abstract Event clone();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public Room getRoom() {
        return room;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subject getCreator() {
        return creator;
    }

    public void setCreator(Subject creator) {
        this.creator = creator;
    }

    public Observer getReactor() {
        return reactor;
    }

    public void setReactor(Observer reactor) {
        this.reactor = reactor;
    }
}

package cz.fel.cvut.omo.smart.home.lehoang.device.visitors;

public enum Action {
    // UNIVERSAL
    TURNON,
    TURNOFF,
    // FRIDGE
    CONSUME,
    PUTIN,
    // TV
    VOLUMEUP,
    VOLUMEDOWN,
    // COOKER, MICROWAVE
    HIGHHEAT,
    MEDIUMHEAT,
    LOWHEAT,
    // PC
    PROGRAMMING,
    // CDPlayer
    PLAYMUSIC,
    // Vacuum Cleaner
    CLEAN,
}

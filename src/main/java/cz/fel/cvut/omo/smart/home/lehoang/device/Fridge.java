package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

public class Fridge extends Device {
    private int foodSum;

    public Fridge(int id, String name, Room room) {
        super(id, name, room);
        this.foodSum = 100;
        turnOn();
    }

    public void consumeFood() {
        if (foodSum > 0) {
            foodSum--;
        }
    }

    public void putFoodInFridge() {
        foodSum++;
    }


    public int getFood() {
        return foodSum;
    }



    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }


}

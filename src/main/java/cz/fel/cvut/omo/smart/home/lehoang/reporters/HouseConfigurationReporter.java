package cz.fel.cvut.omo.smart.home.lehoang.reporters;

import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;
import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;
import cz.fel.cvut.omo.smart.home.lehoang.house.Floor;
import cz.fel.cvut.omo.smart.home.lehoang.house.House;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

public class HouseConfigurationReporter {
    private static final Logger LOG = Logger.getLogger(HouseConfigurationReporter.class.getName());

    private static final String FOLDER_PATH = "src/main/java/cz/fel/cvut/omo/smart/home/lehoang/reporters/reports";

    private static final String FILE_NAME = "house.txt";

    private static final String PATH = FOLDER_PATH +"/"+ FILE_NAME;

    public void create(House house) {
        try {
            File myObj = new File(PATH);
            if (myObj.createNewFile()) {
                LOG.info("File created: " + myObj.getName());
            }
        } catch (
                IOException e) {
            LOG.info("An error occurred.");
            e.printStackTrace();
        }


        try (FileWriter fw = new FileWriter(PATH)) {

            fw.write("+----------------------------+ \n");
            fw.write("| HOUSE CONFIGURATION REPORT | \n");
            fw.write("+----------------------------+ \n");

            fw.write(house.toString() + "\n");

            fw.write("----------------------------------------- \n");

            for (Floor floor : house.getFloors()) {
                fw.write(floor + "\n");

                for (Room room : floor.getRooms()) {
                    fw.write("  " + room + "\n");

                    fw.write("    " + "DEVICES: " + "\n");
                    for (Device device : room.getAllDevices()) {
                        fw.write("      " + device + "\n");
                    }

                    fw.write("    " + "PEOPLE: " + "\n");
                    for (Person person : room.getAllPeople()) {
                        fw.write("      " + person + "\n");
                    }

                    fw.write("    " + "PETS: " + "\n");
                    for (Pet pet : room.getAllPets()) {
                        fw.write("      " + pet + "\n");
                    }
                    fw.write("----------------------------------------- \n");
                }
            }


        } catch (IOException e) {
            LOG.info("Failed writing into file " + FILE_NAME);
            e.printStackTrace();
        }
    }
}

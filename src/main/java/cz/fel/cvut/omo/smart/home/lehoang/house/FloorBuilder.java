package cz.fel.cvut.omo.smart.home.lehoang.house;

public class FloorBuilder {
    private Floor floor;

    public FloorBuilder() {
        reset();
    }

    public FloorBuilder addRoom(Room room){
        this.floor.addRoom(room);
        return this;
    }

    public FloorBuilder setHouse(House house){
        this.floor.setHouse(house);
        return this;
    }

    public FloorBuilder setLevel(int floorNumber){
        floor.setLevel(floorNumber);
        return this;
    }

    public FloorBuilder setName(String name){
        floor.setName(name);
        return this;
    }

    public Floor build(){
        Floor newFloor = new Floor(floor.getName(), floor.getLevel(), floor.getHouse());
        reset();
        return newFloor;
    }

    private void reset() {
        this.floor = new Floor(null, 0, null);
    }
}

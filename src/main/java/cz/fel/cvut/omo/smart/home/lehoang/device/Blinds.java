package cz.fel.cvut.omo.smart.home.lehoang.device;

import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OffState;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OnState;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.DeviceVisitor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.WindSensor;
import cz.fel.cvut.omo.smart.home.lehoang.event.Event;
import cz.fel.cvut.omo.smart.home.lehoang.event.PowerEvent;
import cz.fel.cvut.omo.smart.home.lehoang.event.WindEvent;

public class Blinds extends Device {
    public Blinds(int id, String name, Room room) {
        super(id, name, room);
    }

    @Override
    public void update(Event e) {

        if (e instanceof PowerEvent && this.getState() instanceof OnState) {
            turnOff();

            e.setReactor(this);
            getEventReporter().add(e);

            getActivityAndUsageReporter().addActivity(this.getName() + this.getDeviceId() + " was turned OFF due to blow fuses");
        }

        if (e instanceof WindEvent windevent) {
            if (windevent.getWind() > 50 && this.getState() instanceof OffState) {
                setPowerConsumed(getPowerConsumed() + 0.1);
                turnOn();
                use();
                e.setDescription("were turned ON");
            } else if (windevent.getWind() <= 50 && this.getState() instanceof OnState) {
                turnOff();
                e.setDescription("were turned OFF");
            } else {
                e.setDescription("did nothing");
            }

            WindSensor sensor = this.getRoom().getWindSensor();
            sensor.setWindSpeed(((WindEvent) e).getWind());
            e.setReactor(this);
            getEventReporter().add(e);
        }
    }

    @Override
    public void accept(DeviceVisitor deviceVisitor, Action action) {
        deviceVisitor.visit(this, action);
    }


}

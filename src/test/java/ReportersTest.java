import cz.fel.cvut.omo.smart.home.lehoang.device.Cooker;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.device.Fridge;
import cz.fel.cvut.omo.smart.home.lehoang.device.PC;
import cz.fel.cvut.omo.smart.home.lehoang.device.TV;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.house.Floor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.pets.Pet;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.ActivityAndUsageReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.ConsumptionReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.EventReporter;
import cz.fel.cvut.omo.smart.home.lehoang.reporters.HouseConfigurationReporter;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.PowerSensor;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.TemperatureSensor;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.WindSensor;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.MEDIUMHEAT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReportersTest extends BaseAT {
    private static final String EVENT_TXT_PATH = "src/main/java/cz/fel/cvut/omo/smart/home/lehoang/reporters/reports/event.txt";
    private static final String CONSUMPTION_TXT_PATH = "src/main/java/cz/fel/cvut/omo/smart/home/lehoang/reporters/reports/consumption.txt";
    private static final String ACTIVITYUSAGE_TXT_PATH = "src/main/java/cz/fel/cvut/omo/smart/home/lehoang/reporters/reports/activityUsage.txt";
    private static final String HOUSE_TXT_PATH = "src/main/java/cz/fel/cvut/omo/smart/home/lehoang/reporters/reports/house.txt";

    @Test
    void fridgesIsUsedXTimesAndTxtShowsTheRightAmountOfConsumptionTest() {
        int numberOfActions = 5;
        double kwhConsumedByFridge = ((double) numberOfActions / 10) + 0.1; // formula for kwh
        String formattedKwh = String.format("%.2f", kwhConsumedByFridge);
        String expectedString = "Fridge1: " + formattedKwh;

        String actualString = "";
        double expectedTotalCost = 0;

        Fridge fridge = (Fridge) house.findDeviceByIdAndName(1, "Fridge");
        dad.turnOn(fridge);

        for (int i = 0; i < numberOfActions; i++) {
            dad.putFood(fridge);
        }

        ConsumptionReporter reporter = new ConsumptionReporter();
        reporter.create(house);

        try (BufferedReader reader = new BufferedReader(new FileReader(CONSUMPTION_TXT_PATH))) {
            String line;
            while((line = reader.readLine()) != null) {
                if (line.contains("Fridge1")) {
                    actualString = line;
                } else if (line.contains("Total cost:")) {
                    String totalCostString = line.replaceAll("[^0-9.]", "");
                    expectedTotalCost = Double.parseDouble(totalCostString);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(actualString).isEqualTo(expectedString);
        assertThat(expectedTotalCost).isGreaterThanOrEqualTo(kwhConsumedByFridge);
    }


    @Test
    void houseConfigWillGenerateTxtWithAllFloorsTest() {
        HouseConfigurationReporter houseConfigurationReporter = new HouseConfigurationReporter();
        houseConfigurationReporter.create(house);

        boolean containsAllFloors = true;
        try (BufferedReader reader = new BufferedReader(new FileReader(HOUSE_TXT_PATH))) {
            boolean floorFound = false;
            String line;

            for (Floor floor : house.getFloors()) {
                while ((line = reader.readLine()) != null) {
                    if (line.contains(floor.toString())) {
                        floorFound = true;
                        break;
                    }
                }

                if (!floorFound) {
                    containsAllFloors = false;
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(containsAllFloors).isTrue();
    }

    @Test
    void houseConfigWillGenerateTxtWithAllDevicesTest() {
        HouseConfigurationReporter houseConfigurationReporter = new HouseConfigurationReporter();
        houseConfigurationReporter.create(house);

        boolean containsAllDevices = true;
        try (BufferedReader reader = new BufferedReader(new FileReader(HOUSE_TXT_PATH))) {
            boolean deviceFound = false;
            String line;

            for (Device device : house.getAllDevices()) {
                while ((line = reader.readLine()) != null) {
                    if (line.contains(device.toString())) {
                        deviceFound = true;
                        break;
                    }
                }
                if (!deviceFound) {
                    containsAllDevices = false;
                    break;
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(containsAllDevices).isTrue();
    }

    @Test
    void houseConfigWillGenerateTxtWithAllRoomsTest() {
        HouseConfigurationReporter houseConfigurationReporter = new HouseConfigurationReporter();
        houseConfigurationReporter.create(house);

        boolean containsAllRooms = true;
        try (BufferedReader reader = new BufferedReader(new FileReader(HOUSE_TXT_PATH))) {
            boolean roomFound = false;
            String line;

            for (Room room : house.getAllRooms()) {
                while ((line = reader.readLine()) != null) {
                    if (line.contains(room.toString())) {
                        roomFound = true;
                        break;
                    }
                }
                if (!roomFound) {
                    containsAllRooms = false;
                    break;
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(containsAllRooms).isTrue();
    }

    @Test
    void heaterReactsAccordingToTemperatureTest() {
        Room wohn = house.findRoomByName("Wohnzimmer");
        TemperatureSensor temp1 = wohn.getTemperatureSensor();

        Room schlaf = house.findRoomByName("Elternschlafzimmer");
        TemperatureSensor temp2 = schlaf.getTemperatureSensor();

        Room toilette = house.findRoomByName("Toilette");
        TemperatureSensor temp3 = toilette.getTemperatureSensor();

        temp1.generateEvent();
        temp2.generateEvent();
        temp3.generateEvent();

        EventReporter eventReporter = new EventReporter();
        eventReporter.create(house);

        boolean containsTemperatureEvent = false;
        boolean containsPowerEvent = false;
        try (BufferedReader reader = new BufferedReader(new FileReader(EVENT_TXT_PATH))) {
            String line;

            while ((line = reader.readLine()) != null) {
                if (line.contains("[Temperature Event]")) {
                    containsTemperatureEvent = true;
                    break;
                } else if (line.contains("[Power Event]")) {
                    containsPowerEvent = true;
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(containsTemperatureEvent).isEqualTo(true);
        assertThat(containsPowerEvent).isEqualTo(false);
    }

    @Test
    void generateWindEventBlindsHasToTurnOnOrOff() {
        Room room = house.findRoomByName("Schlafzimmer");
        WindSensor sens = room.getWindSensor();
        sens.generateEvent();

        EventReporter eventReporter = new EventReporter();
        eventReporter.create(house);

        boolean containsTemperatureEvent = false;
        boolean containsWindEvent = false;
        try (BufferedReader reader = new BufferedReader(new FileReader(EVENT_TXT_PATH))) {
            String line;

            while ((line = reader.readLine()) != null) {
                if (line.contains("[Temperature Event]")) {
                    containsTemperatureEvent = true;
                    break;
                } else if (line.contains("[Wind Event]")) {
                    containsWindEvent = true;
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(containsWindEvent).isEqualTo(true);
        assertThat(containsTemperatureEvent).isEqualTo(false);
    }

    @Test
    void activityAndUsageTest() {
        PC pc = (PC)getDevice("PC");
        TV tv = (TV)getDevice("TV");
        Cooker cooker = (Cooker)getDevice("Cooker");
        Fridge fridge = (Fridge)getDevice("Fridge");
        dad.turnOn(pc);
        dad.turnOn(cooker);
        dad.turnOn(tv);
        dad.turnOn(fridge);

        dad.turnVolumeUp(tv);
        dad.cook(cooker, MEDIUMHEAT);
        dad.code(pc);
        dad.putFood(fridge);

        ActivityAndUsageReporter reporter = new ActivityAndUsageReporter();
        reporter.create(house);

        boolean containsAllActions = true;
        try (BufferedReader reader = new BufferedReader(new FileReader(ACTIVITYUSAGE_TXT_PATH))) {
            boolean containsTVAction = false;
            boolean containsCookerAction = false;
            boolean containsFridgeAction = false;
            String line;

            while ((line = reader.readLine()) != null) {
                if (line.contains("turns volume up on TV")) {
                    containsTVAction = true;
                } else if (line.contains("cooks on Cooker")) {
                    containsCookerAction = true;
                } else if (line.contains("puts food into Fridge")) {
                    containsFridgeAction = true;
                }
            }

            if (!containsTVAction || !containsCookerAction || !containsFridgeAction) {
                containsAllActions = false;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(containsAllActions).isEqualTo(true);
    }




}

import cz.fel.cvut.omo.smart.home.lehoang.device.Cooker;
import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OffState;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OnState;
import cz.fel.cvut.omo.smart.home.lehoang.device.PC;
import cz.fel.cvut.omo.smart.home.lehoang.event.TemperatureEvent;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.PowerSensor;
import cz.fel.cvut.omo.smart.home.lehoang.sensor.TemperatureSensor;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import org.junit.jupiter.api.Test;

import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.HIGHHEAT;
import static org.assertj.core.api.Assertions.assertThat;

public class DeviceTest extends BaseAT {


    @Test
    void heaterWillBeTurnedOffIfTemperatureIsLessThan25() {
        Room room = house.findRoomByName("Elternschlafzimmer");
        Device heater = room.findDeviceByIdAndName(1,"Heater");
        TemperatureSensor temperatureSensor = room.getTemperatureSensor();

        TemperatureEvent newEvent = new TemperatureEvent("Temperature Event", room, 24);
        temperatureSensor.notifyObservers(newEvent);

        assertThat(heater.getState().getClass()).isEqualTo(OnState.class);
    }

    @Test
    void heaterWillBeTurnedOnIfTemperatureIs25() {
        Room room = house.findRoomByName("Elternschlafzimmer");
        Device heater = room.findDeviceByIdAndName(1,"Heater");
        TemperatureSensor temperatureSensor = room.getTemperatureSensor();

        TemperatureEvent newEvent = new TemperatureEvent("Temperature Event", room, 25);
        temperatureSensor.notifyObservers(newEvent);

        assertThat(heater.getState().getClass()).isEqualTo(OffState.class);
    }

    @Test
    void consumptionTest() {
        PC pc = (PC)getDevice("PC");
        Cooker cooker = (Cooker)getDevice("Cooker");
        dad.turnOn(pc);
        dad.turnOn(cooker);

        dad.cook(cooker, HIGHHEAT);
        dad.code(pc);

        assertThat(pc.getPowerConsumed()).isLessThan(0.22);
        assertThat(pc.getPowerConsumed()).isGreaterThan(0.2);
        assertThat(cooker.getPowerConsumed()).isEqualTo(2);
    }

    @Test
    void powerEventTest() {

        for (Device device : house.getAllDevices()) {
            device.turnOn();
        }

        PowerSensor pow = house.getAllRooms().get(4).getPowerSensor();
        pow.blowFuse();

        for (Device device : house.getAllDevices()) {
            assertThat(device.getState()).isInstanceOf(OffState.class);
        }

    }



}

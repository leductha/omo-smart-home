import cz.fel.cvut.omo.smart.home.lehoang.device.Device;
import cz.fel.cvut.omo.smart.home.lehoang.house.House;
import cz.fel.cvut.omo.smart.home.lehoang.house.HouseConfig;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;
import cz.fel.cvut.omo.smart.home.lehoang.people.PersonFactory;
import org.junit.jupiter.api.BeforeEach;

import static cz.fel.cvut.omo.smart.home.lehoang.people.PersonType.DAD;

public class BaseAT {
    protected House house;

    protected Person dad;


    @BeforeEach
    void buildHouse() throws Exception {
        HouseConfig houseConfig = new HouseConfig("src/main/resources/house.json");
        house = houseConfig.getHouse();

        PersonFactory personFactory = new PersonFactory();
        Room wohnzimmer = house.findRoomByName("Wohnzimmer");
        dad = personFactory.createPerson(DAD, "Jack", 50, wohnzimmer);
    }

    public Device getDevice(String name) {
        return house.getFloors().stream()
                .flatMap(floor -> floor.getRooms().stream())
                .flatMap(room -> room.getAllDevices().stream())
                .filter(device -> device.getName().equals(name))
                .findFirst()
                .orElse(null);

    }
}

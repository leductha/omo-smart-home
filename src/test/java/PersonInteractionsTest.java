import cz.fel.cvut.omo.smart.home.lehoang.device.Cooker;
import cz.fel.cvut.omo.smart.home.lehoang.device.devicestate.OnState;
import cz.fel.cvut.omo.smart.home.lehoang.device.TV;
import cz.fel.cvut.omo.smart.home.lehoang.device.VacuumCleaner;
import cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action;
import cz.fel.cvut.omo.smart.home.lehoang.house.Room;
import cz.fel.cvut.omo.smart.home.lehoang.people.Person;
import cz.fel.cvut.omo.smart.home.lehoang.people.PersonFactory;
import org.junit.jupiter.api.Test;

import static cz.fel.cvut.omo.smart.home.lehoang.device.visitors.Action.HIGHHEAT;
import static cz.fel.cvut.omo.smart.home.lehoang.people.PersonType.DAD;
import static org.assertj.core.api.Assertions.assertThat;

public class PersonInteractionsTest extends BaseAT{

    @Test
    void ifPersonGoesToRoomHisLocationChangesTest() {
        PersonFactory personFactory = new PersonFactory();
        Room wohnzimmer = house.findRoomByName("Wohnzimmer");
        Room badezimmer = house.findRoomByName("Badezimmer");

        Person dad = personFactory.createPerson(DAD, "Jack", 50, wohnzimmer);
        dad.moveTo(badezimmer);

        assertThat(dad.getRoom()).isEqualTo(badezimmer);
        assertThat(badezimmer.getAllPeople()).anyMatch(person -> person.equals(dad));
    }

    @Test
    void personCooksWithHighHeatWillReturnRightTemperatureTest() {
        Cooker cooker = (Cooker) house.findDeviceByIdAndName(1, "Cooker");

        dad.turnOn(cooker);
        assertThat(cooker.getHeat()).isEqualTo(0);
        dad.cook(cooker, HIGHHEAT);
        assertThat(cooker.getHeat()).isEqualTo(200);
    }

    @Test
    void personCooksWithMediumHeatWillReturnRightTemperatureTest() {
        Cooker cooker = (Cooker) house.findDeviceByIdAndName(1, "Cooker");

        dad.turnOn(cooker);
        assertThat(cooker.getHeat()).isEqualTo(0);
        dad.cook(cooker, Action.MEDIUMHEAT);
        assertThat(cooker.getHeat()).isEqualTo(150);
    }

    @Test
    void personCooksWithLowHeatWillReturnRightTemperatureTest() {
        Cooker cooker = (Cooker) house.findDeviceByIdAndName(1, "Cooker");

        dad.turnOn(cooker);
        assertThat(cooker.getHeat()).isEqualTo(0);
        dad.cook(cooker, Action.LOWHEAT);
        assertThat(cooker.getHeat()).isEqualTo(100);
    }

    @Test
    void personTurnsDeviceOnTest() {
        Cooker cooker = (Cooker) house.findDeviceByIdAndName(1, "Cooker");

        dad.turnOn(cooker);

        assertThat(cooker.getState().getClass()).isEqualTo(OnState.class);
        assertThat(cooker.getTimesUsed()).isEqualTo(0);
    }

    @Test
    void personTurnsTheVolumeOfTVUp() {
        TV tv = (TV)getDevice("TV");

        dad.turnOn(tv);
        dad.turnVolumeUp(tv);
        dad.turnVolumeUp(tv);
        dad.turnVolumeUp(tv);
        dad.turnVolumeUp(tv);

        assertThat(tv.getVolume()).isGreaterThanOrEqualTo(4);
        assertThat(tv.getTimesUsed()).isEqualTo(4);
    }

    @Test
    void vacuumCleanerTest() {
        Room schlafzimmer = house.findRoomByName("Schlafzimmer");
        VacuumCleaner cleaner = (VacuumCleaner) getDevice("VacuumCleaner");
        dad.clean(cleaner, schlafzimmer);

        assertThat(cleaner.getRoom()).isEqualTo(schlafzimmer);
        assertThat(dad.getRoom()).isEqualTo(schlafzimmer);
    }



}
